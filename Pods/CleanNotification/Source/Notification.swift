//
//  Notification.swift
//  CleanNotification
//
//  Created by Lucas Ihlström on 2015-01-31.
//  Copyright (c) 2015 Lucas Ihlström. All rights reserved.
//

import Foundation

public enum ThreadDeliveryPolicy {
    case MainThread
    case SenderThread
    case CustomDispatchQueue(dispatch_queue_t)
    
    private func enterThread(action: () -> ()) {
        switch self {
            case MainThread:
                if NSThread.isMainThread() {
                    fallthrough
                }
                
                dispatch_sync(dispatch_get_main_queue(), action)
            case SenderThread:
                action()
            case CustomDispatchQueue(let queue):
                dispatch_async(queue, action)
        }
    }
}

public class Notification<SenderType, NotificationType where SenderType: AnyObject>: NotificationBase<SenderType, NotificationType> {
    
    private unowned let producer: NotificationProducer<SenderType, NotificationType>
    
    public init(producer: NotificationProducer<SenderType, NotificationType>, deliveryPolicy: ThreadDeliveryPolicy = .MainThread) {
        self.producer = producer
        
        super.init(deliveryPolicy: deliveryPolicy)
        
        producer.install(self)
    }
}

public class NotificationProducer<SenderType, NotificationType where SenderType: AnyObject> {
    
    private weak var notification: Notification<SenderType, NotificationType>?
    
    public init() {}
    
    private func install(notification: Notification<SenderType, NotificationType>) {
        self.notification = notification
    }
    
    public func produce(sender: SenderType, value: NotificationType) {
        notification?.post(sender, value: value)
    }
}

public class OpenNotification<SenderType, NotificationType where SenderType: AnyObject>: NotificationBase<SenderType, NotificationType> {
    
    public override init(deliveryPolicy: ThreadDeliveryPolicy = .MainThread) {
        super.init(deliveryPolicy: deliveryPolicy)
    }
    
    public override func post(sender: SenderType, value: NotificationType) {
        super.post(sender, value: value)
    }
}

public class NotificationBase<SenderType, NotificationType where SenderType: AnyObject> {
    
    public typealias ListenerType = ((sender: SenderType, value: NotificationType) -> ())
    public typealias SenderAwareListenerType = ((value: NotificationType) -> ())
    
    private let lock = Lock()
    
    private let deliveryPolicy: ThreadDeliveryPolicy
    private lazy var listeners = [NotificationListenerBase<SenderType, NotificationType>]()
    
    private init(deliveryPolicy: ThreadDeliveryPolicy) {
        self.deliveryPolicy = deliveryPolicy
    }
    
    public func register(token: AnyObject, listener: ListenerType) {
        register(NotificationListener<SenderType, NotificationType>(token: token, closure: listener))
    }
    
    public func register(listener: ListenerType) {
        register(NotificationListener<SenderType, NotificationType>(closure: listener))
    }
    
    public func registerForSender(token: AnyObject, sender: SenderType, listener: SenderAwareListenerType) {
        register(SenderAwareListener<SenderType, NotificationType>(token: token, sender: sender, closure: listener))
    }
    
    public func registerForSender(sender: SenderType, listener: SenderAwareListenerType) {
        register(SenderAwareListener<SenderType, NotificationType>(sender: sender, closure: listener))
    }
    
    private func register(listener: NotificationListenerBase<SenderType, NotificationType>) {
        lock.lock()
        
        listeners.append(listener)
        
        lock.unlock()
    }
    
    public func unregister(token: AnyObject) -> Bool {
        lock.lock()
        
        if let listener = findListener(token) {
            if let index = listeners.indexOf(listener) {
                listeners.removeAtIndex(index)
                
                lock.unlock()
                
                return true
            }
        }
        
        lock.unlock()
        
        return false
    }
    
    private func findListener(token: AnyObject) -> NotificationListenerBase<SenderType, NotificationType>? {
        for listener in listeners {
            if !listener.anonymous && listener.token === token {
                return listener
            }
        }
        
        return nil
    }
    
    private func post(sender: SenderType, value: NotificationType) {
        deliveryPolicy.enterThread {
            self.lock.lock()
            
            for listener in self.listeners {
                if !listener.alive {
                    continue
                }
                
                listener.notify(sender, value: value)
            }
            
            self.lock.unlock()
        }
    }
}

private class NotificationListener<SenderType, NotificationType where SenderType: AnyObject>: NotificationListenerBase<SenderType, NotificationType> {
    
    private typealias ClosureType = ((sender: SenderType, value: NotificationType) -> ())
    
    private let closure: ClosureType
    
    init(token: AnyObject, closure: ClosureType) {
        self.closure = closure
        
        super.init(token: token)
    }
    
    init(closure: ClosureType) {
        self.closure = closure
        
        super.init()
    }
    
    private override func notify(sender: SenderType, value: NotificationType) {
        closure(sender: sender, value: value)
    }
}

private class SenderAwareListener<SenderType, NotificationType where SenderType: AnyObject>: NotificationListenerBase<SenderType, NotificationType> {
    
    private typealias ClosureType = ((value: NotificationType) -> ())
    
    private weak var sender: SenderType?
    private let closure: ClosureType
    
    init(token: AnyObject, sender: SenderType, closure: ClosureType) {
        self.sender = sender
        self.closure = closure
        
        super.init(token: token)
    }
    
    init(sender: SenderType, closure: ClosureType) {
        self.sender = sender
        self.closure = closure
        
        super.init()
    }
    
    private override func notify(sender: SenderType, value: NotificationType) {
        if self.sender === sender {
            closure(value: value)
        }
    }
}

private class NotificationListenerBase<SenderType, NotificationType where SenderType: AnyObject>: Equatable {
    
    private(set) weak var token: AnyObject?
    let anonymous: Bool
    
    var alive: Bool {
        return anonymous || token != nil
    }
    
    init(token: AnyObject) {
        self.token = token
        
        anonymous = false
    }
    
    init() {
        anonymous = true
    }
    
    func notify(sender: SenderType, value: NotificationType) {
        
    }
}

private func == <SenderType, NotificationType>(lhs: NotificationListenerBase<SenderType, NotificationType>, rhs: NotificationListenerBase<SenderType, NotificationType>) -> Bool {
    return lhs.token === rhs.token
}

//MARK: Operator overloading (Notification)

public struct _PendingNotificationRegistration<SenderType, NotificationType where SenderType: AnyObject> {
    
    private let notification: NotificationBase<SenderType, NotificationType>
    private let token: AnyObject
    
    private func senderAwareRepresentation(sender: SenderType) -> _PendingSenderAwareNotificationRegistration<SenderType, NotificationType> {
        return _PendingSenderAwareNotificationRegistration(notification: notification, token: token, sender: sender)
    }
    
    private func register(listener: (sender: SenderType, value: NotificationType) -> ()) {
        notification.register(token, listener: listener)
    }
}

public struct _PendingSenderAwareNotificationRegistration<SenderType, NotificationType where SenderType: AnyObject> {
    
    private let notification: NotificationBase<SenderType, NotificationType>
    private let token: AnyObject?
    private let sender: SenderType
    
    private func register(listener: (value: NotificationType) -> ()) {
        if let token: AnyObject = self.token {
            notification.registerForSender(token, sender: sender, listener: listener)
        }
        else {
            notification.registerForSender(sender, listener: listener)
        }
    }
}

infix operator <<- { associativity left precedence 150 }

public func <<- <SenderType, NotificationType>(lhs: NotificationBase<SenderType, NotificationType>, rhs: AnyObject) -> _PendingNotificationRegistration<SenderType, NotificationType> {
    return _PendingNotificationRegistration(notification: lhs, token: rhs)
}

infix operator <<<- { associativity left precedence 150 }

public func <<<- <SenderType, NotificationType>(lhs: NotificationBase<SenderType, NotificationType>, rhs: SenderType) -> _PendingSenderAwareNotificationRegistration<SenderType, NotificationType> {
    return _PendingSenderAwareNotificationRegistration(notification: lhs, token: nil, sender: rhs)
}

public func - <SenderType, NotificationType>(lhs: _PendingNotificationRegistration<SenderType, NotificationType>, rhs: SenderType) -> _PendingSenderAwareNotificationRegistration<SenderType, NotificationType> {
    return lhs.senderAwareRepresentation(rhs)
}

public func - <SenderType, NotificationType>(lhs: _PendingNotificationRegistration<SenderType, NotificationType>, rhs: (sender: SenderType, value: NotificationType) -> ()) {
    lhs.register(rhs)
}

public func - <SenderType, NotificationType>(lhs: _PendingSenderAwareNotificationRegistration<SenderType, NotificationType>, rhs: (value: NotificationType) -> ()) {
    lhs.register(rhs)
}

public func <<- <SenderType, NotificationType>(lhs: NotificationBase<SenderType, NotificationType>, rhs: (sender: SenderType, value: NotificationType) -> ()) {
    lhs.register(rhs)
}

public func -= <SenderType, NotificationType>(lhs: NotificationBase<SenderType, NotificationType>, rhs: AnyObject) -> Bool {
    return lhs.unregister(rhs)
}

//MARK: Operator overloading (NotificationProducer)

public struct _PendingNotificationProduction<SenderType, NotificationType where SenderType: AnyObject> {
    
    private let producer: NotificationProducer<SenderType, NotificationType>
    private let sender: SenderType
    
    private func produce(value: NotificationType) {
        producer.produce(sender, value: value)
    }
}

infix operator ->> { associativity left precedence 130 }

public func - <SenderType, NotificationType>(lhs: NotificationProducer<SenderType, NotificationType>, rhs: SenderType) -> _PendingNotificationProduction<SenderType, NotificationType> {
    return _PendingNotificationProduction(producer: lhs, sender: rhs)
}

public func ->> <SenderType, NotificationType>(lhs: _PendingNotificationProduction<SenderType, NotificationType>, rhs: NotificationType) {
    lhs.produce(rhs)
}

//MARK: Operator overloading (OpenNotification)

public struct _PendingOpenNotificationPost<SenderType, NotificationType where SenderType: AnyObject> {
    
    private let notification: OpenNotification<SenderType, NotificationType>
    private let sender: SenderType
    
    private func post(value: NotificationType) {
        notification.post(sender, value: value)
    }
}

public func - <SenderType, NotificationType>(lhs: OpenNotification<SenderType, NotificationType>, rhs: SenderType) -> _PendingOpenNotificationPost<SenderType, NotificationType> {
    return _PendingOpenNotificationPost(notification: lhs, sender: rhs)
}

public func ->> <SenderType, NotificationType>(lhs: _PendingOpenNotificationPost<SenderType, NotificationType>, rhs: NotificationType) {
    lhs.post(rhs)
}
