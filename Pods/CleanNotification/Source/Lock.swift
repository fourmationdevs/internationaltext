//
//  Lock.swift
//  CleanNotification
//
//  Created by Lucas Ihlström on 2015-02-03.
//  Copyright (c) 2015 Lucas Ihlström. All rights reserved.
//

import Foundation

final class Lock: NSObject {
    func lock() {
        if Int(objc_sync_enter(self)) != OBJC_SYNC_SUCCESS {
            fatalError("Failed to lock!")
        }
    }
    
    func unlock() {
        let exitValue = objc_sync_exit(self)
        if Int(exitValue) != OBJC_SYNC_SUCCESS {
            fatalError("Failed to unlock \(exitValue)!")
        }
    }
}
