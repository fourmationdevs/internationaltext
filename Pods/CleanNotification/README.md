# CleanNotification

[![Build Status](https://travis-ci.org/lucasihlstrom/CleanNotification.svg)](https://travis-ci.org/lucasihlstrom/CleanNotification)

CleanNotification is an attempt to make notifications more sensible in Swift.

## Features

- **Type safety**. Both on the notification name and the notification content (sender and value).
- **Easy to use**. A single line to register, unregister and post.
- **Source safety**. The notification can only be posted from within the file it's defined.
- **Thread safety**. Even if the notification is posted on a different thread, it will always be delivered on the expected thread (main thread by default).

## Requirements

- iOS 8.0+ / Mac OS X 10.9+
- Xcode 6.1

## Installation

### CocoaPods

Requires CocoaPods 0.36 (beta)

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '8.0'

pod 'CleanNotification', '~> 1.1'
```

## Example

### Registration

Register for a notification:

```swift
DogDidBarkNotification.register(self) {
    dog, bark in
    println("\(dog.name) barked with decibel: \(bark.decibel) at target: \(bark.target)")
}
```

Or with the custom syntax:

```swift
DogDidBarkNotification <<- self - {
    dog, bark in
    println("\(dog.name) barked with decibel: \(bark.decibel) at target: \(bark.target)")
}
```

You can also register anonymously:

```swift
//Since we're not supplying a token, we cannot unregister later on.
DogDidBarkNotification <<- {
    dog, bark in
    println("\(dog.name) barked with decibel: \(bark.decibel) at target: \(bark.target)")
}
```

### Posting

Post the notification:

```swift
didBarkProducer.produce(self, value: Bark(decibel: 80.0, target: "John Doe"))
```

Or with the custom syntax:

```swift
didBarkProducer - self ->> Bark(decibel: 80.0, target: "John Doe")
```

### Unregistration

Unregister from a notification:

```swift
DogDidBarkNotification.unregister(self)
```

Or with the custom syntax:

```swift
DogDidBarkNotification -= self
```

### Definition

Define a notification as follows:

```swift
private let didBarkProducer = NotificationProducer<Dog, Bark>()
let DogDidBarkNotification = Notification<Dog, Bark>(producer: didBarkProducer)
```

The first generic argument is the sender of the notification (in this case Dog), the second (in this case Bark) is the notification value, think of it as the userInfo.

The producer is what allows the Dog class to be the sole poster of the notification. But if you want any file, anywhere, to be able to post the notification, use an OpenNotification:

```swift
let DogDidBarkNotification = OpenNotification<Dog, Bark>()
```

#### Custom delivery policy

If you want to change what thread the notification is delivered on, supply it it in the constructor:

```swift
let DogDidBarkNotification = Notification<Dog, Bark>(producer: didBarkProducer, deliveryPolicy: .SenderThread)
```

Available delivery policies:

```swift
public enum ThreadDeliveryPolicy {
    case MainThread
    case SenderThread
    case CustomDispatchQueue(dispatch_queue_t)
}
```

### Full example

#### Dog.swift

```swift
struct Bark {
    let decibel: Double
    let target: String
}

private let didBarkProducer = NotificationProducer<Dog, Bark>()
let DogDidBarkNotification = Notification<Dog, Bark>(producer: didBarkProducer)

class Dog {
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func bark(target: String) {
        didBarkProducer - self ->> Bark(decibel: 80.0, target: target)
    }
}
```

#### ViewController.swift

```swift
@IBAction func buttonPressed(sender: UIButton) {
    let dog = Dog("Buddy")
    dog.bark("John Doe")
}

override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
        
    DogDidBarkNotification <<- self - {
        dog, bark in
        println("\(dog.name) barked with decibel: \(bark.decibel) at target: \(bark.target)")
    }
}

override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
    DogDidBarkNotification -= self
}
```

## License

CleanNotification is released under the MIT license. See LICENSE for details.
