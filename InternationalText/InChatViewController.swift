//
//  InChatViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/8/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import JLChatViewController
import Kingfisher

class InChatViewController : JLChatViewController, ChatDataSource, ChatToolBarDelegate, JLChatMessagesMenuDelegate, ChatDelegate  {
	
	
	var thisChat : LocalChats?
	var participants : [LocalFavorites] = [];
	var messages : [Message] = [];
	var recipients : [String] = []
	var groupChat = false;
	let iView : UIImageView! = UIImageView(frame:CGRect(x: 0, y: 0, width: 40, height: 40));
	var loadedIndex = 0;
	
	func setTitle(title:String, subtitle:String) -> UIView {
		//Create a label programmatically and give it its property's
		let titleLabel = UILabel(frame: CGRectMake(0, 0, 0, 0)) //x, y, width, height where y is to offset from the view center
		titleLabel.backgroundColor = UIColor.clearColor()
		titleLabel.textColor = UIColor.blackColor()
		titleLabel.font = UIFont.boldSystemFontOfSize(17)
		titleLabel.text = title
		titleLabel.sizeToFit()
		
		//Create a label for the Subtitle
		let subtitleLabel = UILabel(frame: CGRectMake(0, 18, 0, 0))
		subtitleLabel.backgroundColor = UIColor.clearColor()
		subtitleLabel.textColor = UIColor.lightGrayColor()
		subtitleLabel.font = UIFont.systemFontOfSize(12)
		subtitleLabel.text = subtitle
		subtitleLabel.sizeToFit()
		
		// Create a view and add titleLabel and subtitleLabel as subviews setting
		let titleView = UIView(frame: CGRectMake(0, 0, max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), 30))
		
		// Center title or subtitle on screen (depending on which is larger)
		if titleLabel.frame.width >= subtitleLabel.frame.width {
			var adjustment = subtitleLabel.frame
			adjustment.origin.x = titleView.frame.origin.x + (titleView.frame.width * 0.5) - (subtitleLabel.frame.width * 0.5)
			subtitleLabel.frame = adjustment
		} else {
			var adjustment = titleLabel.frame
			adjustment.origin.x = titleView.frame.origin.x + (titleView.frame.width * 0.5) - (titleLabel.frame.width * 0.5)
			titleLabel.frame = adjustment
		}
		
		titleView.addSubview(titleLabel)
		titleView.addSubview(subtitleLabel)
		
		return titleView
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.layer.insertSublayer(ColorManager.sharedInstance.getBackgroundGradient(self.view), atIndex: 0);
		self.view.backgroundColor = UIColor.clearColor()//ColorManager.sharedInstance.CSBackground;
		self.chatTableView.backgroundColor = self.view.backgroundColor;
		
		
		//let prompt = UINavigationItem(title: "\(thisChat?.chatTitle())");
		//prompt.prompt = "Online"
		//self.navigationItem.titleView = prompt.titleView;
		
		configChat();
		loadTypingViewWithCustomView(nil, animationBlock: nil);
		configToolBar();
		
		iView.sizeThatFits(CGSize(width: 40, height: 40));
		iView.layoutIfNeeded();
		
		NewMessageNotification.register(self) { (sender, value) in
			print("Notification received at: InChat screen");
			
			let id = sender as? String;
			
			if id == self.thisChat?.chatID {
				
				if self.groupChat {
					let fav = self.checkFavorite(value!.senderNumber);
					self.iView.kf_setImageWithURL(NSURL(string: (fav?.smallPictureLink!)!)!, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
						self.iView.layer.borderColor = ColorManager.sharedInstance.getColorforID((fav?.colorId!)!).CGColor;
						value?.senderImage = image;
						self.messages.insert(value!, atIndex: 0);
						self.chatTableView.reloadData();
					})
				} else {
					dispatch_async(dispatch_get_main_queue(), {
						value?.senderImage = self.iView.image;
						let fav = self.checkFavorite(value!.senderNumber);
						if fav != nil {
							self.iView.layer.borderColor = ColorManager.sharedInstance.getColorforID(fav!.colorId != nil ? fav!.colorId! : 12).CGColor;
						}
						self.messages.insert(value!, atIndex: 0);
						self.chatTableView.reloadData();
					})
				}
				

			}
		}
	}
	
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		
		let width = self.view.frame.width;
		let height = self.view.frame.height;
		let view = UIView(frame: CGRect(x: 0, y: 0, width: height, height: width));
		let layer = self.view.layer.sublayers![0];
		self.view.layer.replaceSublayer(layer, with: ColorManager.sharedInstance.getBackgroundGradient(view));
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		//let layer = self.view.layer.sublayers![0];
		//self.view.layer.replaceSublayer(layer, with: ColorManager.sharedInstance.getBackgroundGradient(self.view));
		//self.view.layer.replaceSublayer(ColorManager.sharedInstance.getBackgroundGradient(self.view), atIndex: 0);
	}
	
	override func viewWillDisappear(animated: Bool) {
		// thisChat
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated);
		chatTableView.reloadData();
		
		if thisChat != nil {
			participants = thisChat?.favoritesInChat != nil ? (thisChat?.favoritesInChat)! : [];
			for p in participants {
				recipients.append(p.phoneNumber!);
			}
			
			if recipients.count >= 2 {
				groupChat = true;
			}
			
		}
		
		print("Displaying chat with participants: \(participants.description)\nChat ID: \(thisChat?.chatID)\nChat ID: \(thisChat?.chatID)");
		
		if !groupChat {
			self.navigationItem.title = thisChat?.chatTitle();
			if self.participants[0].smallPictureLink != nil {
				let link = NSURL(string: self.participants[0].smallPictureLink!)!;
				let colorId = participants[0].colorId == nil ? 12 : participants[0].colorId!;
				iView.kf_setImageWithURL(link, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
					dispatch_async(dispatch_get_main_queue() , { 
						self.iView.convertToCirclePicture(ColorManager.sharedInstance.getColorforID(colorId).CGColor, width: 1);
						self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.iView);
					})

				})
			}
		} else {
			self.navigationItem.title = thisChat?.chatTitle();
			//if self.participants[0].smallPictureLink != nil {
			iView.image = UIImage(named: "User Groups-100");
			let colorId = ColorManager.sharedInstance.currentThemeColor;
			iView.convertToCirclePicture(colorId.CGColor, width: 1);
			self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: iView);
		}
		
		loadMessages()
	}
 
	func clickOnPicture() {
		
	}
 
	
	// Chat Source
	func configChat(){
		
		registerCustomCells()
		self.chatTableView.chatDataSource = self
		self.chatTableView.messagesMenuDelegate = self
		self.chatTableView.chatDelegate = self
		self.chatTableView.myID = "0";
		JLChatAppearence.configIncomingMessages(UIColor(hex: "#CCCCCC"), showIncomingSenderImage: true, incomingTextColor: nil)
		JLChatAppearence.configOutgoingMessages(nil/*UIColor(hex: "#")*/, showOutgoingSenderImage: true, outgoingTextColor: nil)
		//JLChatAppearence.configSenderImage(nil, senderImageCornerRadius: nil, senderImageBackgroundColor: nil, senderImageDefaultImage: nil)
		JLChatAppearence.configChatFont(nil) { (indexPath) -> Bool in
			
			
			let correctPosition = self.messages.count - 1 - indexPath.row;
			
			let formatter = NSDateFormatter();
			formatter.dateStyle = .MediumStyle;
			formatter.timeStyle = .ShortStyle;
			formatter.doesRelativeDateFormatting = true;
			
			if correctPosition + 1 >= self.messages.count || correctPosition < 0 {
				return true;
			}
			let new = formatter.stringFromDate(self.messages[correctPosition+1].messageDate);
			let old = formatter.stringFromDate(self.messages[correctPosition].messageDate);
			if new != old {
				return true;
			}
			return false;
		}
	}
	
	func registerCustomCells(){
		self.chatTableView.registerNib(UINib(nibName: "ChatTextCellTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "outgoingCustomCell")
		self.chatTableView.registerNib(UINib(nibName: "incomingCustomCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "incomingCustomCell")
	}
	
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return messages.count;
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		//return tableView.dequeueReusableCellWithIdentifier("")! as UITableViewCell;
		
		//Not sure how this works
		let correctPosition = self.messages.count - 1 - indexPath.row
		
		let message = self.messages[correctPosition]
		
		let messagecell = chatTableView.chatMessageForRowAtIndexPath(indexPath, message: message)
		
		if messagecell is JLImageMessageCell {
			
			//Its here just to simulate the interval of loading the image
			let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(3 * Double(NSEC_PER_SEC)))
			dispatch_after(delayTime, dispatch_get_main_queue()) {
				let imageMess = messagecell as! JLImageMessageCell
				imageMess.addImage(UIImage(named: "imagem2")!)
				message.relatedImage = UIImage(named: "imagem2")!
			}
		}
		return messagecell;
	}
	
	
	
	func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
		let springCell = cell as? ChatTextTableViewCell;
		springCell?.delimiterView.animation = "squeezeLeft"
		springCell?.delimiterView.curve = "spring"
		springCell?.delimiterView.delay = 1.5;
		springCell?.delimiterView.duration = 1.5;
		springCell?.delimiterView.animate();
	}
	
	func chat(chat: JLChatTableView, customMessageCellForRowAtIndexPath indexPath: NSIndexPath) -> JLChatMessageCell {
		
		let correctPosition = self.messages.count - 1 - indexPath.row
		let message = self.messages[correctPosition]
		let prevMsg : Message? = messages.count > correctPosition + 1 ? messages[correctPosition+1] : nil;
		var cell : ChatTextTableViewCell!
		
		
		if message.senderID == self.chatTableView.myID 	{
			cell = self.chatTableView.dequeueReusableCellWithIdentifier("outgoingCustomCell") as! ChatTextTableViewCell
			cell.senderImage.alpha = 1;
			if prevMsg != nil {
				if prevMsg!.senderNumber == message.senderNumber {
					cell.senderImage.alpha = 0;
				}
			}
			cell.senderImage.image = message.senderImage;
			
		} else {
			cell = self.chatTableView.dequeueReusableCellWithIdentifier("incomingCustomCell") as! ChatTextTableViewCell
			cell.senderImage.alpha = 1;
			let dummyfav = LocalFavorites(name: nil, status: nil, smallPictureLink: nil, bigPictureLink: nil, phoneNumber: message.senderNumber, chosenLocale: nil, languageCode: nil, colorId: nil)
			//let index = participants.indexOf(dummyfav);
			
			if prevMsg != nil {
				if prevMsg!.senderNumber == message.senderNumber {
					cell.senderImage.alpha = 0;
				}
			}
			
			if let index = participants.indexOf(dummyfav) {
				print("Chat Participant-Index-Color: \(participants[index].colorId)");
				let colorCG = ColorManager.sharedInstance.getColorforID(participants[index].colorId != nil ? participants[index].colorId! : 12).CGColor
				cell.incomingColor = colorCG;
				cell.delimiterView.layer.borderColor = colorCG;
				cell.senderImage.layer.borderColor = colorCG;
				if self.participants[index].smallPictureLink != nil {
					cell.senderImage.kf_setImageWithURL(NSURL(string: participants[index].smallPictureLink!)!);
				} else {
					cell.senderImage.image = UIImage(named: "Medal-50");
				}
			} else {
				cell.delimiterView.layer.borderColor = UIColor.blackColor().CGColor;
				cell.senderImage.layer.borderColor = UIColor.blackColor().CGColor;
				cell.senderImage.image = UIImage(named: "Medal-50");
			}
		}
		
		cell.backgroundColor = self.view.backgroundColor
		cell.textMessage.text = message.text;
		return cell
	}
	
	func titleforChatLoadingHeaderView() -> String? {
		return "Loading Chat"
	}
	
	func checkFavorite(number: String) -> LocalFavorites? {
		let dummy = LocalFavorites(name: nil, status: nil, smallPictureLink: nil, bigPictureLink: nil, phoneNumber: number, chosenLocale: nil, languageCode: nil, colorId: nil);
		let index = participants.indexOf(dummy);
		
		if index != nil {
			return participants[index!];
		}
		return nil;
	}
	
	func loadMessages() {
		if self.thisChat != nil {
			let dbMessages = DatabaseManager.instance.messagesForChatId((thisChat?.chatID!)!, count: 20, fromIndex: 0);
			if dbMessages.isEmpty == false {
				for message in dbMessages {
					if message.senderID == "0" {
						message.senderImage = UIImage(data: LocalUser.sharedInstance.userData.profilePic!);
						iView.image = message.senderImage;
					} else {
						let p = checkFavorite(message.senderNumber);
						if p != nil {
							if p?.smallPictureLink != nil {
								iView.kf_setImageWithURL(NSURL(string: p!.smallPictureLink!)!);
							} else {
								iView.image = UIImage(named: "Medal-50");
							}
							message.senderImage = iView.image;
						}
					}
					loadedIndex += 1;
					self.messages.insert(message, atIndex: 0);
				}
			}
		}
	}
	
	
	//ChatDelegate
	func loadOlderMessages() {
		print("Trying to load older messages");
		var x = 0;
		let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
		dispatch_after(delayTime, dispatch_get_main_queue()) {
			let dbMessages = DatabaseManager.instance.messagesForChatId((self.thisChat!.chatID!), count: 20, fromIndex: self.loadedIndex)
			if dbMessages.isEmpty == false {
				for msg in dbMessages {
					if msg.senderID == "0" {
						msg.senderImage = UIImage(data: LocalUser.sharedInstance.userData.profilePic!);
						self.iView.image = msg.senderImage;
					} else {
						let p = self.checkFavorite(msg.senderNumber);
						if p != nil {
							self.iView.kf_setImageWithURL(NSURL(string: p!.smallPictureLink!)!);
							msg.senderImage = self.iView.image;
						}
					}
					self.loadedIndex += 1;
					x += 1;
					self.messages.append(msg);
				}
			}
			self.chatTableView.addOldMessages(x);
			//self.chatTableView.setContentOffset(CGPoint(x: 0.0, y: +self.chatTableView.contentInset.top*2), animated: true);
		}
	}
	
	func didTapMessageAtIndexPath(indexPath: NSIndexPath) {
		print("Message Touch");
		let correctMessagePosition = self.messages.count - 1 - indexPath.row;
		let cell = chatTableView.cellForRowAtIndexPath(indexPath) as? ChatTextTableViewCell;
		
		cell?.delimiterView.animation = "flipY";
		cell?.delimiterView.curve = "spring";
		cell?.delimiterView.duration = 0.40;
		
		var string = ""
		for _ in 0..<messages[correctMessagePosition].text!.characters.count {
			string += "  ";
		}
		
		cell?.textMessage.text = string;
		
		cell?.delimiterView.animateToNext({
			
			if let isTrue = cell?.isFlipped {
				if isTrue {
					cell?.textMessage.text = self.messages[correctMessagePosition].text;
				} else {
					cell?.textMessage.text = self.messages[correctMessagePosition].ogText;
				}
				cell?.isFlipped = !(cell?.isFlipped)!;
			}
			
			cell?.textMessage.needsUpdateConstraints();
			cell?.textMessage.updateConstraints();
			//  cell?.delimiterView.needsUpdateConstraints();
			//cell?.delimiterView.updateConstraints()
		})
	}
	
	// MessageMenuDelegate
	func shouldShowMenuItemForCellAtIndexPath(title: String, indexPath: NSIndexPath) -> Bool {
		return false;
	}
	
	func titleForDeleteMenuItem() -> String? {
		return "Delete";
	}
	
	func titleForSendMenuItem() -> String? {
		return "Send again";
	}
	
	func performSendActionForCellAtIndexPath(indexPath: NSIndexPath) {
		
	}
	
	func performDeleteActionForCellAtIndexPath(indexPath: NSIndexPath) {
		
	}
	
	
	// ChatToolBar Menu
	func didTapLeftButton() {
		toolBar.inputText.resignFirstResponder();
	}
	
	func didTapRightButton() {
		
		if self.toolBar.inputText.thereIsSomeText() {
			//let newMessage = JLMessage(text: , senderID: <#T##String#>, messageDate: <#T##NSDate#>, senderImage: <#T##UIImage?#>)
			
			let text = self.toolBar.inputText.text;
			let id = "0"
			let image = UIImage(data: LocalUser.sharedInstance.userData.profilePic!);
			
			let msg = Message(text: text!, senderID: id, messageDate:NSDate(), senderImage: image!);
			msg.ogTag = LocalUser.sharedInstance.userData.languageCode!;
			msg.ogText = text;
			msg.transTag = LocalUser.sharedInstance.userData.languageCode!;
			msg.text = text;
			msg.senderNumber = LocalUser.sharedInstance.userData.phoneNumber!;
			self.messages.insert(msg, atIndex: 0)
			
			let formatter = NSDateFormatter();
			formatter.dateStyle = .MediumStyle;
			formatter.timeStyle = .ShortStyle;
			formatter.doesRelativeDateFormatting = true;
			
			var newRecipients = recipients;
			newRecipients.append(AppDelegate.getAppDelegate().stripPhoneNumber(LocalUser.sharedInstance.userData.phoneNumber!));
			
			SocketHandler.sharedInstance.writeMessageString(text!, to: newRecipients);
			DatabaseManager.instance.insertMessage(msg, chatId: (thisChat?.chatID!)!);
			thisChat?.lastSentMessageDate = formatter.stringFromDate(NSDate());
			
			iView.image = UIImage(data: LocalUser.sharedInstance.userData.profilePic!);
			iView.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
			
			let index = LocalUser.sharedInstance.userData.chats?.indexOf(thisChat!)
			if  index != nil {
				LocalUser.sharedInstance.userData.chats?.insert((LocalUser.sharedInstance.userData.chats?.removeAtIndex(index!))!, atIndex: 0);
			}
		}
		
		
		self.chatTableView.addNewMessage()
		LocalUser.sharedInstance.saveUser();
	}
	
	func configToolBar(){
		toolBar.configToolInputText(UIFont(name: "Helvetica", size: 16)!, textColor: UIColor.blackColor(), placeHolder: "Message")
		toolBar.toolBarDelegate = self
		toolBar.toolBarFrameDelegate = self.chatTableView
		toolBar.configLeftButton("Close", image: nil)
		toolBar.configRightButton("Send", image: nil)
	}
	
	
	
}
