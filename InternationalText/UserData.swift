//
//  UserData.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/14/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import Foundation

class LocalUser {
    
    static let sharedInstance = LocalUser();
    private init() {
		userData = UserData(name: nil, status: nil, languageCode: nil, chosenLocale: nil, imageData: nil, userID: nil, phone: nil, key: nil, favorites: nil, chats: nil, smallPictureLink: nil, bigPictureLink: nil, colorId: nil);
    }
    
    var userData : UserData!
 
    func saveUser() {
        UserData.saveUser(self.userData);
    }
}


class UserData : NSObject, NSCoding {
    
    
    var name : String?
    var statusMessage : String?
    var languageCode : String?
    var profilePic : NSData?
	var chosenLocale : String?
	
    var smallPictureLink : String?
    var bigPictureLink : String?
    
    var userID : Int?
    var phoneNumber : String?
    var secretKey : String?
	
	var colorThemeID : Int?
	
    
    
    var favorites : [LocalFavorites]?
    var chats : [LocalChats]?
    
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!;
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("user");
    
    struct UserDataKey {
        static let name = "username";
        static let statusMessage = "userStatus"
        static let languageCode = "userCode"
        static let profilePic = "picture"
        static let userID = "userId"
        static let phoneNumber = "userPhoneNumber"
        static let secretKey = "secretKey"
        static let favorites = "userFavorites"
        static let chats = "userChats"
        static let smallPictureLink = "smallPictureLink"
        static let bigPictureLink = "bigPictureLink"
		static let chosenLocale = "chosenLocale"
		static let colorThemeID = "colorThemeID"
    }
    
	init(name: String?, status: String?, languageCode: String?, chosenLocale: String?, imageData: NSData?, userID: Int?, phone: String?, key: String?, favorites: [LocalFavorites]?, chats: [LocalChats]?, smallPictureLink: String?, bigPictureLink: String?, colorId: Int?) {
        self.name = name;
        self.statusMessage = status;
        self.languageCode = languageCode;
        self.profilePic = imageData;
        self.userID = userID;
        self.phoneNumber = phone;
        self.secretKey = key;
        self.favorites = favorites;
        self.chats = chats;
        self.smallPictureLink = smallPictureLink;
        self.bigPictureLink = bigPictureLink;
		self.chosenLocale = chosenLocale;
		self.colorThemeID = colorId;
    }
    
    
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObjectForKey(UserDataKey.name) as? String;
        let status = aDecoder.decodeObjectForKey(UserDataKey.statusMessage) as? String;
        let code = aDecoder.decodeObjectForKey(UserDataKey.languageCode) as? String;
        let profilePic = aDecoder.decodeObjectForKey(UserDataKey.profilePic) as? NSData;
		let locale = aDecoder.decodeObjectForKey(UserDataKey.chosenLocale)	as? String;
		
        let id = aDecoder.decodeIntegerForKey(UserDataKey.userID);
        let phone = aDecoder.decodeObjectForKey(UserDataKey.phoneNumber) as? String;
        let secretKey = aDecoder.decodeObjectForKey(UserDataKey.secretKey) as? String;
        let favorites = aDecoder.decodeObjectForKey(UserDataKey.favorites) as? [LocalFavorites]
        let chats = aDecoder.decodeObjectForKey(UserDataKey.chats) as? [LocalChats];
        let smallPictureLink = aDecoder.decodeObjectForKey(UserDataKey.smallPictureLink) as? String;
        let bigPictureLink = aDecoder.decodeObjectForKey(UserDataKey.bigPictureLink) as? String;
		let colorId = aDecoder.decodeObjectForKey(UserDataKey.colorThemeID) as? Int;
        
        self.init(name: name, status: status, languageCode: code, chosenLocale: locale, imageData: profilePic, userID: id, phone: phone, key: secretKey, favorites: favorites, chats: chats, smallPictureLink: smallPictureLink, bigPictureLink: bigPictureLink, colorId: colorId);
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
     
        aCoder.encodeObject(name, forKey: UserDataKey.name);
        aCoder.encodeObject(statusMessage, forKey: UserDataKey.statusMessage);
        aCoder.encodeObject(languageCode, forKey: UserDataKey.languageCode);
        aCoder.encodeObject(profilePic, forKey: UserDataKey.profilePic);
		aCoder.encodeObject(chosenLocale, forKey: UserDataKey.chosenLocale);
        aCoder.encodeInteger(userID!, forKey: UserDataKey.userID);
        aCoder.encodeObject(phoneNumber, forKey: UserDataKey.phoneNumber);
        aCoder.encodeObject(secretKey, forKey: UserDataKey.secretKey);
        aCoder.encodeObject(favorites, forKey: UserDataKey.favorites);
        aCoder.encodeObject(chats, forKey: UserDataKey.chats);
        aCoder.encodeObject(smallPictureLink, forKey: UserDataKey.smallPictureLink);
        aCoder.encodeObject(bigPictureLink, forKey:  UserDataKey.bigPictureLink);
		aCoder.encodeObject(colorThemeID, forKey: UserDataKey.colorThemeID);
    }
    
    class func saveUser(user: UserData) {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(user, toFile: UserData.ArchiveURL.path!);
        if isSuccessfulSave == false {
            print("Failed to save User...");
        } else {
            print("User saved...");
        }
    }
    
    class func loadUser() -> UserData? {
        return NSKeyedUnarchiver.unarchiveObjectWithFile(UserData.ArchiveURL.path!) as? UserData;
    }
	
	class func syncWithDatabase() {
		
		RemoteData.getMyUser({ (data) in
			
			
			
			
			
			
			
		})
		
		
		
	}
	
	
	
	
	
	
	
    
}