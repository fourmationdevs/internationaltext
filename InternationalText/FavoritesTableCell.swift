//
//  FavoritesTableCell.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/22/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit

class FavoritesTableCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var pictureView: UIImageView!
    
	@IBOutlet weak var languageLabel: UILabel!
	@IBOutlet weak var flagView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		pictureView.layer.borderWidth = 5;
		pictureView.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
		
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setFlag(code: String) {
		
		let name = "SwiftCountryPicker.bundle/Images/\(code.uppercaseString)";
		let image = UIImage(named: name, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)
		print(image);
		
		flagView.image = image;
	}
	
	func setLanguage(locale: String) {
		
		RemoteData.requestAndPullAvailableLanguages { (data) in
			let json : JSON = JSON(data: data);
			
			
			if json["success"] != nil {
				if json["success"].bool! {
					let array = json["languages"].array!
					
					for language in array {
						if language["code"].string == locale {
							let string = language["name"].string!;
							
							dispatch_async(dispatch_get_main_queue(), { 
								self.languageLabel!.text = string;
							})
						}
					}
				}
			}
		}
		
	}
	
}
