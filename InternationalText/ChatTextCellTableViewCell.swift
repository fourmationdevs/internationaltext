//
//  ChatTextCellTableViewCell.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/9/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import JLChatViewController
import Spring

class ChatTextTableViewCell:  JLChatMessageCell {

    @IBOutlet weak var delimiterView: SpringView!
    @IBOutlet weak var senderImage: UIImageView!
    
    @IBOutlet weak var textMessage: UILabel!
    @IBOutlet weak var textFlag: UILabel!
    
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var errorToSendButton: UIButton!
    
    @IBOutlet weak var errorToSendButtonLeadingDistance: NSLayoutConstraint!
    
    @IBOutlet weak var senderImageHeight: NSLayoutConstraint!
    @IBOutlet weak var senderImageWidth: NSLayoutConstraint!
	
	var incomingColor : CGColor?
	
	
	var isFlipped = false;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		/*
		let image = UIImage(named: "JLChatViewController.bundle/alert9Normal.png", inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil);
		errorToSendButton.setBackgroundImage(image, forState: .Normal);
		*/
		
		
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    override internal func initCell(message:JLMessage,thisIsNewMessage:Bool,showDate:Bool,isOutgoingMessage:Bool){
        
        super.initCell(message, thisIsNewMessage: thisIsNewMessage, showDate: showDate, isOutgoingMessage: isOutgoingMessage)
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        configView()
        
        //let productMessage = message as! ProductMessage
        self.textMessage.text = message.text
        self.senderImage.image = message.senderImage
        
        if message.messageStatus == MessageSendStatus.ErrorToSend{
            showErrorButton(false)
        }
        
        if showDate{
            self.dateText.text = message.generateStringFromDate()//"Wednesday - 12/12/2015"
        }
        else{
            dateText.text = nil
        }
        //If the cell is being reused do not config these things again
        if cellAlreadyUsed == false{
            
            self.textMessage.font = JLChatAppearence.chatFont
            
            if isOutgoingMessage{
                configAsOutgoingMessage()
            }
            else{
                configAsIncomingMessage()
            }
            cellAlreadyUsed = true
        }
    }
    
    
    private func  configView(){
        delimiterView.layer.masksToBounds = true
        delimiterView.layer.cornerRadius = self.frame.height * 0.15
        delimiterView.layer.borderWidth = 2
		//delimiterView.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
		senderImage.layer.cornerRadius = senderImage.frame.height * 0.5;
		senderImage.layer.borderWidth = 2 ;
		senderImage.clipsToBounds = true;
        //senderImage.convertToCirclePicture(ColorManager.sharedInstance.currentThemeColor.CGColor, width: 1);
		self.contentView.backgroundColor = UIColor.clearColor();
    }

    
    override func updateMessageStatus(message:JLMessage){
        super.updateMessageStatus(message)
        if message.messageStatus == MessageSendStatus.ErrorToSend {
            self.showErrorButton(true)
        }
        else{
            self.hideErrorButton(true)
        }
    }
    
    //MARK: - Alert error button methods
    
    override func showErrorButton(animated:Bool){
        
        super.showErrorButton(animated)
        
        self.errorToSendButtonLeadingDistance.constant = 5
        
        if animated{
            
            UIView.animateWithDuration(0.4) { () -> Void in
                self.layoutIfNeeded()
            }
            
            UIView.animateWithDuration(0.5, delay: 0.3, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.errorToSendButton.alpha = 1
                
                }, completion: nil)
        }
        else{
            self.errorToSendButton.alpha = 1
            self.layoutIfNeeded()
        }
        
        
    }
    
    override internal func hideErrorButton(animated:Bool){
        
        super.hideErrorButton(animated)
        self.errorToSendButtonLeadingDistance.constant = -35
        
        if animated{
            UIView.animateWithDuration(0.4) { () -> Void in
                self.layoutIfNeeded()
            }
            UIView.animateWithDuration(0.4, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.errorToSendButton.alpha = 0
                
                }, completion: nil)
        }
        else{
            self.errorToSendButton.alpha = 0
            self.layoutIfNeeded()
        }
    }
    
    
    //MARK: - Menu methods
    @IBAction func errorButtonAction(sender: AnyObject) {
        showMenu()
    }
    
    override func configMenu(deleteTitle:String?,sendTitle:String?,deleteBlock:()->(),sendBlock:()->()){
        
        if !isMenuConfigured{
            addLongPress()
        }
        
        super.configMenu(deleteTitle, sendTitle: sendTitle, deleteBlock: deleteBlock, sendBlock: sendBlock)
    }
    
    
    private func addLongPress(){
        
        let longPress = UILongPressGestureRecognizer(target: self, action:#selector(ChatTextTableViewCell.longPressAction(_:)))
        
        self.delimiterView.addGestureRecognizer(longPress)
        
    }
    
    
    func longPressAction(longPress:UILongPressGestureRecognizer) {
        
        if longPress.state == UIGestureRecognizerState.Began {
            self.delimiterView.alpha = 0.5
        }
        else if longPress.state == UIGestureRecognizerState.Ended {
            self.showMenu()
        }
        else if longPress.state == UIGestureRecognizerState.Cancelled || longPress.state == UIGestureRecognizerState.Failed{
            self.delimiterView.alpha = 1
        }
        
    }
    
    
    func showMenu(){
        
        self.becomeFirstResponder()
        self.delimiterView.alpha = 1
        let targetRectangle = self.delimiterView.frame
        UIMenuController.sharedMenuController().setTargetRect(targetRectangle, inView: self)
        UIMenuController.sharedMenuController().setMenuVisible(true, animated: true)
    
    }
    
    
    
    //MARK: - Config methods
    internal override func configAsOutgoingMessage(){
        
        super.configAsOutgoingMessage()
        
        if JLChatAppearence.showOutgoingSenderImage{
            self.senderImage.backgroundColor = JLChatAppearence.senderImageBackgroundColor
			self.senderImage.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
            self.senderImageHeight.constant = 32//JLChatAppearence.senderImageSize.height
            self.senderImageWidth.constant = 32//JLChatAppearence.senderImageSize.width
            self.senderImage.layer.cornerRadius = 16//JLChatAppearence.senderImageCornerRadius
        }
        else{
            self.senderImageHeight.constant = 0
            self.senderImageWidth.constant = 0
        }
        self.textMessage.font = JLChatAppearence.chatFont
        self.textMessage.textColor = JLChatAppearence.outGoingTextColor
        self.delimiterView.backgroundColor = JLChatAppearence.outgoingBubbleColor
		self.delimiterView.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
        
    }
    
    override internal func configAsIncomingMessage(){
        
        super.configAsIncomingMessage()
        
        if JLChatAppearence.showIncomingSenderImage{
            self.senderImage.backgroundColor = JLChatAppearence.senderImageBackgroundColor
            self.senderImageHeight.constant = 32//JLChatAppearence.senderImageSize.height
            self.senderImageWidth.constant = 32//JLChatAppearence.senderImageSize.width
            self.senderImage.layer.cornerRadius = 16//JLChatAppearence.senderImageCornerRadius
            
        }
        else{
            self.senderImageHeight.constant = 0
            self.senderImageWidth.constant = 0
        }
        
        self.textMessage.font = JLChatAppearence.chatFont
        self.textMessage.textColor = JLChatAppearence.incomingTextColor
        self.delimiterView.backgroundColor = JLChatAppearence.incomingBubbleColor
		self.delimiterView.layer.borderColor = incomingColor != nil ? incomingColor! : UIColor.blackColor().CGColor;
        
    }

    
    
    
    
}
