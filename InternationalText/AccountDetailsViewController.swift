//
//  AccountDetailsViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/9/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import Spring

extension UIView {
	
	func convertToCirclePicture(color: CGColor, width: CGFloat) {
		
		self.layer.cornerRadius = self.frame.height * 0.5;
		self.layer.borderColor = color;
		self.layer.borderWidth = width;
		self.clipsToBounds = true;
		
	}
	
}

class AccountDetailsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
	
	//Color Buttons array
	@IBOutlet var colorButtonsArray: [SpringButton]!
	@IBOutlet weak var ColorContainer: UIView!
	
	let colorsArray = [
		ColorManager.sharedInstance.CSRed,
		ColorManager.sharedInstance.CSOrange,
		ColorManager.sharedInstance.CSLightOrange,
		ColorManager.sharedInstance.CSYellow,
		ColorManager.sharedInstance.CSLime,
		ColorManager.sharedInstance.CSGreen,
		ColorManager.sharedInstance.CSMagenta,
		ColorManager.sharedInstance.CSPink,
		ColorManager.sharedInstance.CSPurple,
		ColorManager.sharedInstance.CSBlue,
		ColorManager.sharedInstance.CSLightBlue,
		ColorManager.sharedInstance.CSTeal
	]
	
	var currentSelectedColor = 0;
	
	//Fields
	@IBOutlet weak var signUpButton: SpringButton!
	@IBOutlet weak var displayName: UITextField!
	@IBOutlet weak var statusMessage: UITextField!
	@IBOutlet weak var profileImageView: UIImageView!
	@IBOutlet weak var LanguageButton: UIButton!
	@IBOutlet weak var errorLabel: UILabel!
	@IBOutlet weak var infoLabel: UILabel!
	
	
	//Connection
	let serverURL = ""
	var availableLanguages : JSON?
	var selectedLanguage : String = ""
	
	
	//Camera and Variables
	let imagePicker = UIImagePickerController();
	//let fastCamera = FastttCamera();
	var phoneNumber : String!;
	var languageCode = "";
	var hasALanguageBeenSet : Bool = false;
	var hasANameBeenSet = false;
	var hasAColorBeenSet = false;
	
	override func viewDidLoad() {
		super.viewDidLoad()
		//		let CMG = ColorManager.sharedInstance;
		let tap = UITapGestureRecognizer(target: self, action: #selector(tapProfilePic));
		let tap2 = UITapGestureRecognizer(target: self, action: #selector(tapBackground));
		self.view.addGestureRecognizer(tap2);
		profileImageView.addGestureRecognizer(tap);
		profileImageView.convertToCirclePicture(UIColor.whiteColor().CGColor, width: 3);
		imagePicker.delegate = self;
		displayName.delegate = self;
		
		//Check
		
		if LocalUser.sharedInstance.userData.name != nil {
			displayName.text = LocalUser.sharedInstance.userData.name;
			hasANameBeenSet = true;
		}
		
		if LocalUser.sharedInstance.userData.languageCode != nil {
			languageCode = LocalUser.sharedInstance.userData.languageCode!;
			LanguageButton.setTitle("Language: \(languageCode.uppercaseString)", forState: .Normal);
			hasALanguageBeenSet = true;
			checkIfDone()
		}
		
		if LocalUser.sharedInstance.userData.profilePic != nil {
			profileImageView.image = UIImage(data: LocalUser.sharedInstance.userData.profilePic!);
		}
		
		LanguageButton.layer.cornerRadius = 5;
		infoLabel.layer.cornerRadius = 5;
		
		//colorTouch.doesNotRecognizeSelector(#selector(colorButtonTouch));
		
		
		for (index, button) in colorButtonsArray.enumerate() {
			button.backgroundColor = colorsArray[index];
			button.layer.borderWidth = 3;
			button.layer.cornerRadius = button.frame.height * 0.5;
			
		}
		
		ColorContainer.layer.borderWidth = 4;
		ColorContainer.layer.cornerRadius = 5;
		
		infoLabel.layer.cornerRadius = 5;
		//infoLabel.layer.borderWidth = 3;
		
		
		
	}
	
	override func viewWillAppear(animated: Bool) {
		for (index, button) in colorButtonsArray.enumerate() {
			let colorTouch = UITapGestureRecognizer(target: self, action: #selector(colorButtonTouch))
			colorButtonsArray[index].addGestureRecognizer(colorTouch);
			//print(button.gestureRecognizers);
			
			
			button.animation = "squeezeUp";
			button.animation = "spring";
			button.duration = 1;
			button.delay = 0.1 * CGFloat(index);
			button.animate();
		}
		
		updateUIColors();
	}
	
	override func unwindForSegue(unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
		print("unwind");
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	// Actions ///////////////////////////////////////
	@IBAction func unwindWithSelectedLanguage(segue:UIStoryboardSegue) {
		if let languagePickerViewController = segue.sourceViewController as? LanguageTableViewController {
			self.selectedLanguage = languagePickerViewController.languagueSelected;
			self.languageCode = languagePickerViewController.languageCode;
			LanguageButton.titleLabel?.text = selectedLanguage;
			hasALanguageBeenSet = true;
			//errorLabel.hidden = true;
		}
	}
	
	@IBAction func signUp(sender: UIButton) {
		
		let name = displayName.text;
		let status = statusMessage.text;
		
		//f name?.isEmpty == false {
		//	if hasALanguageBeenSet {
		//		errorLabel.hidden = true;
		
		LocalUser.sharedInstance.userData.name = name!;
		LocalUser.sharedInstance.userData.statusMessage = status;
		LocalUser.sharedInstance.userData.languageCode = languageCode;
		
		if profileImageView.image != nil {
			LocalUser.sharedInstance.userData.profilePic = UIImageJPEGRepresentation(profileImageView.image!, 1.0);
			RemoteData.uploadPicture();
		}
		LocalUser.sharedInstance.saveUser();
		
		RemoteData.sendUserData({ (data) in
			dispatch_async(dispatch_get_main_queue(), {
				self.performSegueWithIdentifier("accountToMainSegue", sender: self);
			})
		})
		
	}
	
	
	func tapProfilePic() {
		print("Tap tap");
		createActionSheet();
	}
	
	func tapBackground() {
		print("tap on background");
		displayName.resignFirstResponder();
		statusMessage.resignFirstResponder();
	}
	
	func colorButtonTouch(sender: UITapGestureRecognizer) {
		
		print("Color tapped")
		print(sender.view?.tag);
		
		colorButtonsArray[currentSelectedColor].layer.borderColor = UIColor.blackColor().CGColor;
		sender.view?.layer.borderColor = UIColor.whiteColor().CGColor;
		
		currentSelectedColor = sender.view!.tag;
		ColorManager.sharedInstance.setCurrentUIColor(currentSelectedColor);
		hasAColorBeenSet = true;
		updateUIColors();
	}
	
	func updateUIColors() {
		profileImageView.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
		ColorContainer.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
		ColorContainer.layer.borderWidth = 1;
		signUpButton.backgroundColor = ColorManager.sharedInstance.currentThemeColor;
		checkIfDone();
	}
	
	func checkIfDone() {
		
		if hasANameBeenSet && hasAColorBeenSet && hasALanguageBeenSet {
			signUpButton.hidden = false;
			signUpButton.animation = "slideUp";
			signUpButton.curve = "easeIn";
			signUpButton.duration = 1;
			signUpButton.damping = 1;
			signUpButton.animate();
			
		} else {
			signUpButton.hidden = true;
		}
		
	}
	
	///////////////////////////////////////
	
	func textFieldDidEndEditing(textField: UITextField) {
		if textField.text?.isEmpty == false {
			hasANameBeenSet = true;
		} else {
			hasANameBeenSet = false;
		}
		
		checkIfDone();
	}
	
	func createActionSheet() {
		
		// 1
		let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .ActionSheet)
		
		// 1.5 the iPad step
		if let popoverPresentationController = optionMenu.popoverPresentationController {
			popoverPresentationController.sourceView = profileImageView;
			popoverPresentationController.sourceRect = profileImageView.frame;
		}
		
		// 2
		let pickImageAction = UIAlertAction(title: "Choose from Photo Library", style: .Default, handler: {
			(alert: UIAlertAction!) -> Void in
			print("Photo Library")
			self.imagePicker.allowsEditing = true
			self.imagePicker.sourceType = .PhotoLibrary;
			//self.imagePicker.mediaTypes = [UTType ];
			
			self.presentViewController(self.imagePicker, animated: true, completion: nil)
			
		})
		let takePictureAction = UIAlertAction(title: "Take Photo", style: .Default, handler: {
			(alert: UIAlertAction!) -> Void in
			print("Take Photo")
			self.imagePicker.allowsEditing = true;
			self.imagePicker.sourceType = .Camera;
			self.imagePicker.cameraDevice = .Front;
			
			
			self.presentViewController(self.imagePicker, animated: true, completion: nil);
			//self.dismissViewControllerAnimated(false, completion: nil);
		})
		
		//
		let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
			(alert: UIAlertAction!) -> Void in
			print("Cancelled")
			//self.dismissViewControllerAnimated(false, completion: nil);
		})
		
		
		// 4
		optionMenu.addAction(pickImageAction)
		optionMenu.addAction(takePictureAction)
		optionMenu.addAction(cancelAction)
		
		// 5
		self.presentViewController(optionMenu, animated: true, completion: nil)
	}
	
	func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
		
		let image = info[UIImagePickerControllerEditedImage] as? UIImage;
		profileImageView.image = image;
		picker.dismissViewControllerAnimated(true, completion: nil);
		
	}
	
	func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
		print("Did finish picking image");
		
		profileImageView.image = image;
		picker.dismissViewControllerAnimated(true, completion: nil);
		
	}
	
	
}
