//
//  ColorSelectViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 7/11/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import Spring
import Kingfisher

class ColorSelectViewController: UIViewController {

	@IBOutlet weak var dummyProfileView: UIImageView!
	@IBOutlet weak var dummyChatView: UIImageView!
	@IBOutlet weak var dummyTextBubble: UIView!
	@IBOutlet weak var colorContainer: UIView!
	@IBOutlet var colorArray: [SpringButton]!
	
	
	var currentSelectedColor = 0;
	var hasAColorBeenSet = false;
	
    override func viewDidLoad() {
        super.viewDidLoad()

		self.view.backgroundColor = ColorManager.sharedInstance.CSBackground;
        // Do any additional setup after loading the view.
		dummyProfileView.convertToCirclePicture(ColorManager.sharedInstance.currentThemeColor.CGColor, width: 2);
		dummyProfileView.image = UIImage(data: LocalUser.sharedInstance.userData.profilePic!);
		dummyChatView.image = UIImage(data: LocalUser.sharedInstance.userData.profilePic!);
		
		dummyChatView.convertToCirclePicture(ColorManager.sharedInstance.currentThemeColor.CGColor, width: 2);
		dummyTextBubble.layer.borderWidth = 2;
		dummyTextBubble.layer.cornerRadius = dummyTextBubble.frame.height * 0.25;
		dummyTextBubble.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
		
		
		colorContainer.layer.borderWidth = 2;
		colorContainer.layer.cornerRadius = colorContainer.frame.height * 0.2;
		colorContainer.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		for (index, button) in colorArray.enumerate() {
			let colorTouch = UITapGestureRecognizer(target: self, action: #selector(colorButtonTouch))
			colorArray[index].addGestureRecognizer(colorTouch);
			colorArray[index].convertToCirclePicture(UIColor.blackColor().CGColor, width: 2);
			//print(button.gestureRecognizers);
			
			
			button.animation = "squeezeUp";
			button.animation = "spring";
			button.duration = 1;
			button.delay = 0.1 * CGFloat(index);
			button.animate();
		}
	}

	func colorButtonTouch(sender: UITapGestureRecognizer) {
		print("Color tapped")
		print(sender.view?.tag);
		
		colorArray[currentSelectedColor].layer.borderColor = UIColor.blackColor().CGColor;
		sender.view?.layer.borderColor = UIColor.whiteColor().CGColor;
		
		currentSelectedColor = sender.view!.tag;
		ColorManager.sharedInstance.setCurrentUIColor(currentSelectedColor);
		hasAColorBeenSet = true;
		tabBarController?.tabBar.tintColor = ColorManager.sharedInstance.currentThemeColor;
		updateUIColors();
	}
	
	
	func updateUIColors() {
		dummyProfileView.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
		colorContainer.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
		dummyChatView.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
		dummyTextBubble.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
