//
//  LanguageTableViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/9/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit

class LanguageTableViewController: UITableViewController {

    
    var languages = [String]();
    var serverURL : String = API_ADDRESS + "get_available_languages?locale=";
    
    var jsonData : JSON!
    var jsonLanguageArray : [JSON] = []
    
    var languagueSelected : String = ""
    var languageCode : String = "";

    
    override func viewDidLoad() {
        super.viewDidLoad()
        RemoteData.requestAndPullAvailableLanguages { (data) in
            self.jsonData = JSON(data: data);
            
            //print(self.jsonData);
            
            if self.jsonData["success"].bool! {
                self.jsonLanguageArray = self.jsonData["languages"].array!;
                dispatch_async(dispatch_get_main_queue(), {
                    self.reloadTable();
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadTable() {
        self.tableView.reloadData();
	}
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return jsonLanguageArray.count;
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LanguageSelectionCell", forIndexPath: indexPath)

        // Configure the cell...
        
        cell.textLabel?.text = jsonLanguageArray[indexPath.row]["name"].string;
        cell.detailTextLabel?.text = jsonLanguageArray[indexPath.row]["code"].string;
    
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //languagueSelected = jsonLanguageArray[indexPath.row]["name"].string!;
        
        
        print(languagueSelected);
        //navigationController?.popViewControllerAnimated(true);
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "returnLanguage" {
            guard let cell = sender as? UITableViewCell else {
                return;
            }
            guard let indexPath = tableView.indexPathForCell(cell) else {
                return
            }

            languagueSelected = jsonLanguageArray[indexPath.row]["name"].string!
            languageCode = jsonLanguageArray[indexPath.row]["code"].string!
        }
    }

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}