//
//  AppDelegate.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/6/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import Contacts

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var contactStore = CNContactStore();


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        if UserData.loadUser() != nil {
            LocalUser.sharedInstance.userData = UserData.loadUser()
			
			if LocalUser.sharedInstance.userData.colorThemeID != nil {
				ColorManager.sharedInstance.setCurrentUIColor(LocalUser.sharedInstance.userData.colorThemeID!);
			}
			
			print("UserData exist");
            
        } else {
            LocalUser.sharedInstance.userData = UserData(name: nil, status: nil, languageCode: nil, chosenLocale: nil, imageData: nil, userID: nil, phone: nil, key: nil, favorites: nil, chats: nil, smallPictureLink: nil, bigPictureLink: nil, colorId: nil);
        }
        
		
		DatabaseManager.instance.displayEntries();   
		
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate;
    }
    
    func showMessage(message: String) {
        let alertController = UIAlertController(title: "Render", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
        }
        
        alertController.addAction(dismissAction)
        
        let pushedViewControllers = (self.window?.rootViewController as! UINavigationController).viewControllers
        let presentedViewController = pushedViewControllers[pushedViewControllers.count - 1]
        
        presentedViewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func stripPhoneNumber(rawString: String) -> String {
        var  strippedString = "";
        let scanner = NSScanner(string: rawString);
        let numbers = NSCharacterSet(charactersInString: "0123456789")
        
        while scanner.atEnd != true {
            var buffer : NSString?
            if scanner.scanCharactersFromSet(numbers, intoString: &buffer) {
                strippedString.appendContentsOf(buffer! as String);
            } else {
                scanner.scanLocation = scanner.scanLocation + 1;
            }
        }
        
        //print(strippedString);
        
        return strippedString;
    }

}

