//
//  SocketHandler.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/20/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import Foundation
import Starscream
import CleanNotification
import LNRSimpleNotifications
import AudioToolbox
import ImageIO

private let didReceivedAMessage = NotificationProducer<AnyObject, Message?>();
let NewMessageNotification = Notification<AnyObject, Message?>(producer: didReceivedAMessage);
let notificationManager = LNRNotificationManager();

class SocketHandler : WebSocketDelegate {
    static let sharedInstance = SocketHandler();
	let socket = WebSocket(url: (NSURL(string: WEBSOCKET_ADDRESS))!);
	
	
    private init() {
        socket.delegate = self;
        socket.connect();
        //autheticateWithITserver();
		//let desc = UIFontDescriptor(name: "Avenir Next", size: 21);
		
		
		notificationManager.notificationsPosition = LNRNotificationPosition.Top
		notificationManager.notificationsBackgroundColor = ColorManager.sharedInstance.currentThemeColor;
		notificationManager.notificationsTitleTextColor = UIColor.blackColor()
		notificationManager.notificationsTitleFont = notificationManager.notificationsTitleFont.fontWithSize(18);
		notificationManager.notificationsBodyTextColor = UIColor.blackColor()
		notificationManager.notificationsBodyFont = notificationManager.notificationsBodyFont.fontWithSize(14);
		notificationManager.notificationsSeperatorColor = UIColor.grayColor()
		
		
		let alertSoundURL: NSURL? = NSBundle.mainBundle().URLForResource("click", withExtension: "wav")
		if let _ = alertSoundURL {
			var mySound: SystemSoundID = 0
			AudioServicesCreateSystemSoundID(alertSoundURL!, &mySound)
			notificationManager.notificationSound = mySound
		}
		//return true
    }
	
	
    
    func autheticateWithITserver() {
		print("userId: \(LocalUser.sharedInstance.userData.userID)")
        let params = ["userId" : LocalUser.sharedInstance.userData.userID!, "secretKey" : LocalUser.sharedInstance.userData.secretKey!, "action" : "authenticate"];
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted);
            self.writeString(NSString(data: data, encoding: NSUTF8StringEncoding)! as String);
        } catch let error {
            print(error);
        }
    }
    
    func writeMessageString(text: String, to: [String]) {
        let params = ["userId" : LocalUser.sharedInstance.userData.userID!, "secretKey" : LocalUser.sharedInstance.userData.secretKey!, "text" : text, "phoneNumbers" : to, "action" : "create_message"];
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted);
            self.writeString(NSString(data: data, encoding: NSUTF8StringEncoding)! as String);
        } catch let error {
            print(error);
        }
    }
    
    func websocketDidConnect(socket: WebSocket) {
        print("websocket is connected!");
        autheticateWithITserver();
    }
    
    func websocketDidDisconnect(socket: WebSocket, error: NSError?) {
        print("websocket is disconnected: \(error?.localizedDescription)");
		
		
		socket.connect();
		
    }
    
    func websocketDidReceiveMessage(socket: WebSocket, text: String) {
        print("Websocket: DidReceiveMessage");
        
        
        let json = JSON(data: text.dataUsingEncoding(NSUTF8StringEncoding)!);
        print(json.description);
        
        if let action : String = json["action"].string {
			
            if action == "get_new_messages" {
				handleNewMessages(socket, json: json);
			} else if action == "authenticate" {
				if json["authenticated"].bool == false {
					
					if json["message"].string! == "Invalid userId." {
						//AppDelegate
					} else {
						autheticateWithITserver();
					}
					
					
				}
			}
        }
    }
    
    func websocketDidReceiveData(socket: WebSocket, data: NSData) {
        print("got some data: \(data.length)");
    }
    
    
    func writeData(data: NSData) {
        socket.writeData(data);
    }
    
    func writeString(string: String) {
        
        socket.writeString(string);
    }
    
    func disconnect() {
        socket.disconnect();
    }
    
    func isConnected() -> Bool {
        return socket.isConnected;
    }
	
	
	private func handleNewMessages(socket: WebSocket, json: JSON) {
		
		if json["newMessages"].array!.isEmpty == false {
			
			for messages in json["newMessages"].array! {
				
				let text = messages["translatedText"].string!;
				let senderId = "1";
				let original = messages["originalText"].string!;
				let incCode = messages["originalLanguage"].string!;
				

				
				let formatter = NSDateFormatter();
				formatter.doesRelativeDateFormatting = true;
				formatter.dateStyle = .MediumStyle;
				formatter.timeStyle = .ShortStyle;
				let dateString = formatter.stringFromDate(NSDate());

				let sender = messages["senderPhoneNumber"].string!;
				var numbers = messages["phoneNumbers"].arrayObject as! [String];
				
				let phoneNumber = AppDelegate.getAppDelegate().stripPhoneNumber(LocalUser.sharedInstance.userData.phoneNumber!);
				if sender != phoneNumber {
					numbers.append(sender);
					//Duplicate killer
					numbers = Array(Set(numbers));
				}
				//Duplicate killer
				numbers = Array(Set(numbers));
				let i = numbers.indexOf(phoneNumber);
				
				let message = Message(text: text, senderID: senderId, messageDate: NSDate(), senderImage: nil);
				message.ogTag = incCode;
				message.transTag = LocalUser.sharedInstance.userData.languageCode!;
				message.ogText = original;
				message.senderNumber = sender;
				
				

				let dummyFav = LocalFavorites(name: nil, status: nil, smallPictureLink: nil, bigPictureLink: nil, phoneNumber: sender, chosenLocale: nil, languageCode: nil, colorId: nil);
				if LocalUser.sharedInstance.userData.favorites!.contains(dummyFav) {
					
				}
				
				
				if i != nil {
					if numbers.count > 1 {
						numbers.removeAtIndex(i!);
					}
					
					numbers.sortInPlace { $0 < $1 };
					
					var id : String = ""
					
					for n in numbers {
						id += n;
					}
					
					DatabaseManager.instance.insertMessage(message, chatId: id);
					
					let chats : [LocalChats]? = LocalUser.sharedInstance.userData.chats;
					let searchChat = LocalChats(participants: nil, favoritesInChat: nil, lastSentMessage: nil, languages: nil, chatID: id);
					
					if chats != nil {
						// There's chats saved
						if chats!.contains(searchChat) {
							//Found the chat
							let index = chats!.indexOf(searchChat)!
							LocalUser.sharedInstance.userData.chats![index].lastSentMessageDate = dateString;
							LocalUser.sharedInstance.userData.chats?.insert((LocalUser.sharedInstance.userData.chats?.removeAtIndex(index))!, atIndex: 0);
							
						} else {
							//Chat doesn't exist
							print("Chat doesn't exist. Creating chat and adding message.")
							let dummyChat = LocalChats(participants: numbers, favoritesInChat: nil, lastSentMessage: dateString, languages: nil, chatID: id);
							LocalUser.sharedInstance.userData.chats!.insert(dummyChat, atIndex: 0);
						}
					} else {
						//No chats, create one
						print("No chats exist. Creating and adding message.")
						//let message = LocalMessages(message: message);
						let dummyChat = LocalChats(participants: numbers, favoritesInChat: nil, lastSentMessage: dateString, languages: nil, chatID: id);
						LocalUser.sharedInstance.userData.chats = [dummyChat];
					}
					
					LocalUser.sharedInstance.saveUser();
					// Let know to the current conversation that a new message exist
					didReceivedAMessage.produce(id, value: message);
					checkUser(sender, message: message);
					
				}
			}
		}
	}
	
	private func checkUser(number: String, message: Message) {
		RemoteData.checkNumber(number, completion: { (user) in
			self.triggerNotification(user, message: message);
		})
	}
	
	private func triggerNotification(user: LocalFavorites, message: Message) {
		
		let url = NSURL(string: user.smallPictureLink != nil ? user.smallPictureLink! : "");
		if let imagesource = CGImageSourceCreateWithURL(url!, nil) {
			let options : [NSString : NSObject] = [
				kCGImageDestinationImageMaxPixelSize: 64,
				kCGImageSourceCreateThumbnailFromImageAlways: true
			]
			
			let scaledImage = CGImageSourceCreateImageAtIndex(imagesource, 0, options).flatMap {UIImage(CGImage: $0)};
			notificationManager.notificationsIcon = scaledImage;
		}
		
		
		let name = user.name;
		//let view = UIImageView(image: UIImage(named: "Medal-50"));
		//if user.smallPictureLink != nil {
		//	view.kf_setImageWithURL(NSURL(string: user.smallPictureLink!)!);
			
		//}
		
		//notificationManager.notificationsIcon = view.image;
		notificationManager.notificationsBackgroundColor = UIColor.blackColor();//  ColorManager.sharedInstance.currentThemeColor;
		notificationManager.showNotification(name == nil ? "Render:" : name! + ":", body: message.text!) { _ in
			print("Notification sent");
			notificationManager.dismissActiveNotification({ () -> Void in
				print("Notification dismissed")
			})
		}
	}
}
