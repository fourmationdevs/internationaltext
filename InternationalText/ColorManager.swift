//
//  ColorManager.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 7/7/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit

class ColorManager {
	
	static let sharedInstance = ColorManager();
	private init() {}
	
	var currentThemeId : Int = 0;
	var currentThemeColor : UIColor = UIColor.redColor()
	
	
	// UI Important Colors
	static let CSBackground_hex = "#04448E"
	let CSBackground = UIColor(hex: CSBackground_hex);
	


	
	func getBackgroundGradient(view: UIView) -> CAGradientLayer {
		
		let color1 = UIColor(hex: "#5aa0d3").CGColor;
		let color2 = UIColor(hex: "#79bbdb").CGColor;
		let color3 = UIColor(hex: "#9cd1eb").CGColor;
		let color4 = UIColor(hex: "#79bbdb").CGColor;
		let color5 = UIColor(hex: "#5aa0d3").CGColor;
		let CSGradient = CAGradientLayer();
		CSGradient.frame = view.bounds;
		CSGradient.colors = [color1, color2, color3, color4, color5];
		CSGradient.locations = [0.0, 0.25, 0.5, 0.75, 1];
		
		return CSGradient;
	}
	
	
	//HexValue
	static let CSRed_hex = "#cc0000"
	let CSRed = UIColor(hex: CSRed_hex)
	
	//HexValue
	static let CSOrange_hex = "#F6BFD0"
	let CSOrange = UIColor(hex: CSOrange_hex)
	
	//HexValue
	static let CSLightOrange_hex = "#F7DD6E"
	let CSLightOrange = UIColor(hex: CSLightOrange_hex)
	
	//HexValue
	static let CSYellow_hex = "#AED964"
	let CSYellow = UIColor(hex: CSYellow_hex)
	
	//HexValue
	static let CSLime_hex = "#99cccc"
	let CSLime = UIColor(hex: CSLime_hex)
	
	//HexValue
	static let CSGreen_hex = "#00cccc"
	let CSGreen = UIColor(hex: CSGreen_hex)
	
	//HexValue
	static let CSMagenta_hex = "#0033cc"
	let CSMagenta = UIColor(hex: CSMagenta_hex);
	
	//HexValue
	static let CSPink_hex = "#CAB5ED"
	let CSPink = UIColor(hex: CSPink_hex);

	//HexValue
	static let CSPurple_hex = "#996699"
	let CSPurple = UIColor(hex: CSPurple_hex);
	

	//HexValue
	static let CSBlue_hex = "#DCF0E4"
	let CSBlue = UIColor(hex: CSBlue_hex)
	
	//HexValue
	static let CSLightBlue_hex = "#ff9900"
	let CSLightBlue = UIColor(hex: CSLightBlue_hex)
	
	//HexValue
	static let CSTeal_hex = "#666666"
	let CSTeal = UIColor(hex: CSTeal_hex)
	
	
	func getColorforID(id: Int ) -> UIColor {
		let color : UIColor!
		
		switch id {
			
		case 0:
			color = CSRed
		case 1:
			color = CSOrange
		case 2:
			color = CSLightOrange
		case 3:
			color = CSYellow
		case 4:
			color = CSLime
		case 5:
			color = CSGreen
		case 6:
			color = CSMagenta
		case 7:
			color = CSPink
		case 8:
			color = CSPurple
		case 9:
			color = CSBlue
		case 10:
			color = CSLightBlue
		case 11:
			color = CSTeal
		default:
			print("Default Hit")
			color = UIColor.blackColor();
		}
		
		return color;
		
	}
	
	func setCurrentUIColor(id : Int) {
		
		currentThemeId = id;
		
		switch id {
			
		case 0:
			currentThemeColor = CSRed
		case 1:
			currentThemeColor = CSOrange
		case 2:
			currentThemeColor = CSLightOrange
		case 3:
			currentThemeColor = CSYellow
		case 4:
			currentThemeColor = CSLime
		case 5:
			currentThemeColor = CSGreen
		case 6:
			currentThemeColor = CSMagenta
		case 7:
			currentThemeColor = CSPink
		case 8:
			currentThemeColor = CSPurple
		case 9:
			currentThemeColor = CSBlue
		case 10:
			currentThemeColor = CSLightBlue
		case 11:
			currentThemeColor = CSTeal
			
		default:
			print("Default Hit")
		}
		
		LocalUser.sharedInstance.userData.colorThemeID = currentThemeId;
		LocalUser.sharedInstance.saveUser();
		
	}
}