//
//  FavoritesTableViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/16/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import JLChatViewController
import Contacts

class FavoritesTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
	
	var favs = LocalUser.sharedInstance.userData.favorites != nil ? LocalUser.sharedInstance.userData.favorites! : [];
	var favsSearch = [LocalFavorites]();
	
	
	@IBOutlet weak var tableView: UITableView!
	let searchController = UISearchController(searchResultsController: nil);
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.registerNib(UINib(nibName: "FavoritesTableCell", bundle: nil), forCellReuseIdentifier: "favoritesCell");
		tableView.separatorInset.left = 58;
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		let edit = self.editButtonItem();
		edit.action = #selector(beginEditing);
		self.navigationItem.rightBarButtonItem = edit;
		
		let refresh = UIRefreshControl()
		refresh.addTarget(self, action: #selector(refreshTable), forControlEvents: .ValueChanged);
		tableView.addSubview(refresh);
		
		
		searchController.searchResultsUpdater = self;
		searchController.dimsBackgroundDuringPresentation = false;
		definesPresentationContext = true;
		tableView.tableHeaderView = searchController.searchBar;
		
	}
	
	override func viewWillAppear(animated: Bool) {
		favs = LocalUser.sharedInstance.userData.favorites != nil ? LocalUser.sharedInstance.userData.favorites! : [];
		print(favs);
		tableView.reloadData();
	}
	
	func refreshTable(refreshController: UIRefreshControl) {
		favs = LocalUser.sharedInstance.userData.favorites != nil ? LocalUser.sharedInstance.userData.favorites! : [];
		var numbers : [String] = [];
		var names : [String] = []
		for f in favs {
			numbers.append(f.phoneNumber!);
			names.append(f.name!);
		}
		
		RemoteData.checkForNumbers(numbers, contactNames: names, completion: {() in });
		tableView.reloadData();
		refreshController.endRefreshing();
	}
	
	func beginEditing() {
		tableView.editing = !tableView.editing;
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		var numOfSections = 0;
		
		if (favs.count > 0)
		{
			self.tableView.separatorStyle = .SingleLine;
			numOfSections                 = 1;
			tableView.backgroundView   = nil;
		}
		else
		{
			let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
			noDataLabel.text = "No favorites added :(\nTry checking or adding contacts!";
			noDataLabel.textColor = UIColor.blackColor();
			noDataLabel.textAlignment = .Center;
			tableView.backgroundView = noDataLabel;
			tableView.separatorStyle = .None;
		}
		
		return numOfSections;
	}
	
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		
		if searchController.active && searchController.searchBar.text != "" {
			return favsSearch.count;
		}
		return favs.count;
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 48;
	}
	
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("favoritesCell", forIndexPath: indexPath) as! FavoritesTableCell
		
		let fav : LocalFavorites!
		if searchController.active && searchController.searchBar.text != "" {
			fav = favsSearch[indexPath.row];
		} else {
			fav = favs[indexPath.row];
		}
		
		// Configure the cell...
		cell.nameLabel?.text = fav.name;
		let check = fav.status != nil
		let check2 = fav.status == ""
		cell.statusLabel?.text =  check && !check2 ? fav.status! : "Hey there! I'm using IT."
	
		
		cell.pictureView?.convertToCirclePicture( fav.colorId != nil ? ColorManager.sharedInstance.getColorforID(fav.colorId!).CGColor : UIColor.blackColor().CGColor, width: 1);
		
		cell.setFlag(fav.chosenLocale!);
		cell.setLanguage(fav.languageCode!);
		
		if fav.smallPictureLink != nil {
			let url = NSURL(string: fav.smallPictureLink!);
			cell.pictureView.contentMode = .ScaleAspectFit
			cell.pictureView?.kf_setImageWithURL(url!);
		} else {
			cell.pictureView.contentMode = .Center
			cell.pictureView.image = UIImage(named: "Medal-50");
			//cell.pictureView?.hnk_setImage(UIImage(named: "Medal-50")!, key: "default");
		}
		
		return cell
	}
	
	func filterContactsSearch(searchText: String) {
		favsSearch = favs.filter { fav in
			let fullname = fav.name!.lowercaseString;
			let result = fullname.containsString(searchText.lowercaseString)
			return result;
		}
		tableView.reloadData();
	}
	
	
	func updateSearchResultsForSearchController(searchController: UISearchController) {
		filterContactsSearch(searchController.searchBar.text!);
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if searchController.active && searchController.searchBar.text != "" {
			openChat(favsSearch[indexPath.row]);
		} else {
			openChat(favs[indexPath.row]);
		}
	}
	
	
	func openChat(chat: LocalFavorites) {
		//use this for you present the JLChatViewController
		JLBundleController.loadJLChatStoryboard();
		if let vc = JLBundleController.instantiateJLChatVC() as? InChatViewController {
			vc.view.frame = self.view.frame
			let chatSegue = UIStoryboardSegue(identifier: "favToChat", source: self, destination: vc, performHandler: { () -> Void in
				//vc.participants = [chat];
				// self.presentViewController(vc, animated: true, completion: nil);
				self.navigationController?.pushViewController(vc, animated: true);
				//self.presentViewController(vc, animated: true, completion: nil);
			})
			self.prepareForSegue(chatSegue, sender: chat)
			chatSegue.perform()
		}
	}
	
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		print("Preparing Segue: \(segue.identifier)");
		
		if segue.identifier == "favToChat" {
			let chat = sender as? LocalFavorites;
			let vc = segue.destinationViewController as? InChatViewController
			//vc?.participants = [chat!];
			
			
			
			
			let newChat = LocalChats(participants: [chat!.phoneNumber!], favoritesInChat: [chat!], lastSentMessage: nil, languages: nil, chatID: chat!.phoneNumber);
			
			if LocalUser.sharedInstance.userData.chats != nil {
				if LocalUser.sharedInstance.userData.chats!.contains(newChat) {
					let index = LocalUser.sharedInstance.userData.chats!.indexOf(newChat);
					vc?.thisChat = LocalUser.sharedInstance.userData.chats![index!];
				} else {
					LocalUser.sharedInstance.userData.chats?.append(newChat);
					vc?.thisChat = LocalUser.sharedInstance.userData.chats?.last;
				}
			} else {
				LocalUser.sharedInstance.userData.chats = [newChat];
				vc?.thisChat = LocalUser.sharedInstance.userData.chats?.last;
			}
			
			LocalUser.sharedInstance.saveUser();
		}
	}
	
	func tableView(tableView: UITableView, shouldIndentWhileEditingRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return false;
	}
	
	func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
		return .None;
	}
	
	
	// Override to support conditional editing of the table view.
	func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	// Return false if you do not want the specified item to be editable.
		return true
	}

	
	
	// Override to support editing the table view.
	func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
		if editingStyle == .Delete {
			// Delete the row from the data source
			//tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
		} else if editingStyle == .Insert {
			// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
		}
	}
	
	
	
	// Override to support rearranging the table view.
	func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
		
		let fav = favs.removeAtIndex(fromIndexPath.row);
		favs.insert(fav, atIndex: toIndexPath.row);
		tableView.reloadData();
		LocalUser.sharedInstance.userData.favorites = favs;
		LocalUser.sharedInstance.saveUser();
		
	}
	
	
	
	// Override to support conditional rearranging of the table view.
	func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		// Return false if you do not want the item to be re-orderable.
		return true
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
