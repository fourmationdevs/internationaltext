//
//  SelectGroupMembersTableViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 7/19/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import Kingfisher
import JLChatViewController

class SelectGroupMembersTableViewController: UITableViewController {
	
	var favs = LocalUser.sharedInstance.userData.favorites != nil ? LocalUser.sharedInstance.userData.favorites! : [];
	var favsSearch : [LocalFavorites] = [];
	let searchController = UISearchController(searchResultsController: nil);
	var selectedRows : [Int] = [];
	
	var selectedFavs : [LocalFavorites] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.separatorInset.left = 58;
		tableView.registerNib(UINib(nibName: "FavoritesTableCell", bundle: nil), forCellReuseIdentifier: "favoritesCell");
		
		tableView.allowsSelection = true;
		
		tableView.tableHeaderView = searchController.searchBar;
		
		let createButton = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: #selector(createGroupButton));
		self.navigationItem.rightBarButtonItem = createButton;
		
		
		
		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		// self.navigationItem.rightBarButtonItem = self.editButtonItem()
	}
	
	override func viewWillAppear(animated: Bool) {
		favs = LocalUser.sharedInstance.userData.favorites != nil ? LocalUser.sharedInstance.userData.favorites! : [];
	}
	
	func createGroupButton() {
		print("[SelectGroupMembersTableViewController]: Create group button pressed.");
		print("Selected Members: \(selectedFavs)");
		
		var selectedNumbers : [String] = []
		for s in selectedFavs {
			selectedNumbers.append(s.phoneNumber!);
		}
		
		selectedNumbers.sortInPlace { $0 < $1 };
		
		let newGroup = LocalChats(participants: selectedNumbers, favoritesInChat: selectedFavs, lastSentMessage: nil, languages: nil, chatID: nil);
		LocalUser.sharedInstance.userData.chats?.append(newGroup);
		openChat(newGroup);
	}
	
	func openChat(chat: LocalChats) {
		JLBundleController.loadJLChatStoryboard();
		if let vc = JLBundleController.instantiateJLChatVC() as? InChatViewController {
			vc.view.frame = self.view.frame
			let chatSegue = UIStoryboardSegue(identifier: "favToChat", source: self, destination: vc, performHandler: { () -> Void in
				//vc.participants = [chat];
				// self.presentViewController(vc, animated: true, completion: nil);
				self.navigationController?.pushViewController(vc, animated: true);
				//self.presentViewController(vc, animated: true, completion: nil);
			})
			self.prepareForSegue(chatSegue, sender: chat)
			chatSegue.perform()
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 2
	}
	
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if section == 0 {
			return "Selected";
		} else {
			return "Favorites";
		}
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		
		if section == 0 {
			return selectedFavs.count;
		}
		
		return favs.count;
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("favoritesCell", forIndexPath: indexPath) as! FavoritesTableCell
		
		var fav : LocalFavorites?
		
		if indexPath.section == 1 {
			
			if searchController.active && searchController.searchBar.text != "" {
				fav = favsSearch[indexPath.row];
			} else {
				fav = favs[indexPath.row];
			}
		} else {
			fav = selectedFavs[indexPath.row];
		}
		
		
		//cell.layer.borderWidth = 2;
		//cell.layer.borderColor = UIColor.greenColor().CGColor;
		
		
		// Configure the cell...
		cell.nameLabel?.text = fav?.name;
		cell.statusLabel?.text = fav!.status != nil || fav!.status == "" ? fav!.status! : "Hey there! I'm using Render."
		
		
		cell.pictureView?.convertToCirclePicture( fav!.colorId != nil ? ColorManager.sharedInstance.getColorforID(fav!.colorId!).CGColor : UIColor.blackColor().CGColor, width: 1);
		
		cell.setFlag(fav!.chosenLocale!);
		cell.setLanguage(fav!.languageCode!);
		
		if fav!.smallPictureLink != nil {
			let url = NSURL(string: fav!.smallPictureLink!);
			cell.pictureView.contentMode = .ScaleAspectFit
			//cell.pictureView.hnk_cancelSetImage();
			cell.pictureView?.kf_setImageWithURL(url!);
		} else {
			cell.pictureView.contentMode = .Center
			cell.pictureView.image = UIImage(named: "Medal-50");
			//cell.pictureView?.hnk_setImage(UIImage(named: "Medal-50")!, key: "default");
		}
		
		
		
		return cell
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if indexPath.section == 0 {
			selectedFavs.removeAtIndex(indexPath.row);
			//selectedRows.removeAtIndex(indexPath.row);
		} else if indexPath.section == 1 {
			
			if selectedFavs.contains(favs[indexPath.row]) {
				selectedFavs.removeAtIndex(selectedFavs.indexOf(favs[indexPath.row])!);
			} else {
				selectedFavs.append(favs[indexPath.row]);
			}
		}
		
		tableView.reloadData();
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		
		if let chat = sender as? LocalChats {
			
			let vc = segue.destinationViewController as? InChatViewController
			vc?.thisChat = chat;
			dismissViewControllerAnimated(true, completion: nil);
		}
		
	}
	
	/*
	// Override to support conditional editing of the table view.
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	// Return false if you do not want the specified item to be editable.
	return true
	}
	*/
	
	/*
	// Override to support editing the table view.
	override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
	if editingStyle == .Delete {
	// Delete the row from the data source
	tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
	} else if editingStyle == .Insert {
	// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
	}
	*/
	
	/*
	// Override to support rearranging the table view.
	override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
	
	}
	*/
	
	/*
	// Override to support conditional rearranging of the table view.
	override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	// Return false if you do not want the item to be re-orderable.
	return true
	}
	*/
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
