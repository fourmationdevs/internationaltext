//
//  ChatTableViewCell.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/7/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var chFriendName: UILabel!
    @IBOutlet weak var chLastMessage: UILabel!
    
    @IBOutlet weak var chTimeLastMesssage: UILabel!
    
    @IBOutlet weak var chStatusLastMessage: UILabel!
    
    @IBOutlet weak var chFriendAvatar: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("Initializing Cell")
		chFriendAvatar.layer.borderWidth = 2;
		chFriendAvatar.layer.cornerRadius = chFriendAvatar.frame.size.width/2;
		//print(chFriendAvatar.frame.size);
		//print(chFriendAvatar.frame.size.width * 0.25);
		//print(chFriendAvatar.frame.size.width * 0.5);
		
		chFriendAvatar.clipsToBounds = true;
    }
    
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
