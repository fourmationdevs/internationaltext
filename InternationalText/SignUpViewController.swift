//
//  SignUpViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/7/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import Spring
import MRCountryPicker

class SignUpViewController: UIViewController, MRCountryPickerDelegate, UITextFieldDelegate {

    @IBOutlet weak var countryPicker: UIPickerView!
    @IBOutlet weak var areaCodeField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var confirmButtom: UIButton!
	@IBOutlet weak var errorLabel: UILabel!
    
    
    @IBOutlet weak var pickerContainer: SpringView!
    @IBOutlet weak var mrCountryPicker: MRCountryPicker!
    
    var sortedCountryArray : [(name: String, code: String)] = []
    var areacodeJsonUrl = "http://country.io/phone.json";
    var areaCodes : JSON?
	var chosenLocale = "";
    
    @IBAction func confirmPress(sender: UIButton) {
        
        phoneNumberField.resignFirstResponder();
		errorLabel.hidden = true;
        // Send phone number to server.
        
        let aCode = areaCodeField.text;
        
        let number = phoneNumberField.text;
        
        if aCode?.isEmpty == false {
            if number?.isEmpty == false {
                let numberString = aCode! + number!;
                
                let dialog = UIAlertController(title: "Phone Number Confirmation", message: "We will send a verification code to \(numberString)", preferredStyle: .Alert)
                
                let alertAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil);
                let alertConfirm = UIAlertAction(title: "Confirm", style: .Default, handler: { (action) in
                    
                    if numberString.characters.count <= 5 {
                        return;
                    }
                    else {
                        //Code to request verification text
                        RemoteData.sendPhoneNumber(numberString, completion: { (data) -> Void in
							
							
							if data["success"] == true || data["exists"] == true {
								if data["userId"] != 0 {
									LocalUser.sharedInstance.userData.userID = data["userId"].int!;
									LocalUser.sharedInstance.userData.phoneNumber = numberString;
									LocalUser.sharedInstance.userData.chosenLocale = self.chosenLocale;
									
									LocalUser.sharedInstance.saveUser();
									//Move next screen
									dispatch_async(dispatch_get_main_queue(), {
										self.performSegueWithIdentifier("verificationSegue", sender: self);
									})
								}
							} else {
								
								dispatch_async(dispatch_get_main_queue(), {
									self.errorLabel.hidden = false;
								})
							}
                        });
                    }
                })
				
                dialog.addAction(alertAction);
                dialog.addAction(alertConfirm);
                self.presentViewController(dialog, animated: true, completion: nil);
            }
        }
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        countryCode();
//        //var areaCodes : JSON?
//        areaCodeRetrieve { (jsonData) in
//            self.areaCodes = JSON(data: jsonData!);
//            
//            if let code = self.areaCodes!["US"].string {
//                print(code);
//            }
//        }
//        
//        //countryPicker.dataSource = self;
//        //countryPicker.delegate = self;
//        // Do any additional setup after loading the view.
		
		
		phoneNumberField.delegate = self;
        mrCountryPicker.countryPickerDelegate = self;
        mrCountryPicker.showPhoneNumbers = true;
        
        let locale = NSLocale.currentLocale()
        let code = locale.objectForKey(NSLocaleCountryCode) as! String
        mrCountryPicker.setCountry(code)
    }
    
    override func viewWillAppear(animated: Bool) {
        pickerContainer.animate();
    }
    
    override func viewDidAppear(animated: Bool) {
        storageCheck();
    }
	
	func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
		pickerContainer.animation = "fall"
		pickerContainer.curve = "easeIn"
		pickerContainer.duration = 1.0
		pickerContainer.animate();

		return true;
	}
	
    func textFieldDidBeginEditing(textField: UITextField) {
        
		
        
    }
	
	func textFieldShouldEndEditing(textField: UITextField) -> Bool {
		pickerContainer.animation = "slideUp"
		pickerContainer.curve = "easeIn"
		pickerContainer.duration = 1.0;
		pickerContainer.delay = 0.1;
		pickerContainer.animate();
		
		return true;
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "verificationSegue" {
            
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        phoneNumberField.resignFirstResponder();
    }
    
    func countryPhoneCodePicker(picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        areaCodeField.text = phoneCode;
		chosenLocale = countryCode;
        
    }
    
    func storageCheck() {
        
        if LocalUser.sharedInstance.userData.phoneNumber == nil {
            return;
        }
        
        if LocalUser.sharedInstance.userData.phoneNumber != nil && LocalUser.sharedInstance.userData.userID != nil {
            if LocalUser.sharedInstance.userData.secretKey != nil {
                if LocalUser.sharedInstance.userData.name != nil && LocalUser.sharedInstance.userData.languageCode != nil {
                    //Everything is fine
                    performSegueWithIdentifier("entryToMainSegue", sender: self);
                } else {
                    performSegueWithIdentifier("confirmationToAccountSegue", sender: self);
                }
            } else {
                // User has not finished verificating.
                RemoteData.sendPhoneNumber(LocalUser.sharedInstance.userData.phoneNumber!, completion: { data in
                    if data["userId"] != 0 {
                        LocalUser.sharedInstance.userData.userID = data["userId"].int!;
                        //LocalUser.sharedInstance.userData.phoneNumber = data["];
                        LocalUser.sharedInstance.saveUser();
                        //Move next screen
                        dispatch_async(dispatch_get_main_queue(), {
                            self.performSegueWithIdentifier("verificationSegue", sender: self);
                        })
                    }
                });
                
            }
        }
        
        return;
    }
    
    /*
     func countryCode() {
     
     let locale = NSLocale.currentLocale();
     let countryArray = NSLocale.ISOCountryCodes();
     
     var unsortedCountryArray : [(name: String, code: String)] = [];
     
     for countryCode in countryArray {
     
     //print(countryCode);
     
     let displayNameString =     locale.displayNameForKey(NSLocaleCountryCode, value: countryCode);
     if displayNameString != nil {
     unsortedCountryArray.append((displayNameString!, countryCode));
     }
     }
     
     sortedCountryArray = unsortedCountryArray.sort { $0.name < $1.name };
     
     }
     
     func areaCodeRetrieve(success: ((jsonData: NSData!) -> Void )) {
     
     RemoteData.loadDataFromURL(NSURL(string: areacodeJsonUrl)!) { (data, error) in
     
     if let resultData = data {
     success(jsonData: resultData);
     }
     
     if error != nil {
     print(error);
     }
     
     
     }
     
     }
     */
}
