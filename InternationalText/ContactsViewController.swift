//
//  ContactsViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/6/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class ContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CNContactPickerDelegate, UISearchResultsUpdating {
    
    var contacts = [CNContact]();
    @IBOutlet weak var tableView: UITableView!
    
    var sections : [(index: Int, length: Int, title: String)] = [];
    
    
    //Search Variable
    let searchController = UISearchController(searchResultsController: nil);
    var contactsSearch = [CNContact]();
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
        tableView.delegate = self;
        tableView.dataSource = self;
        
        var status = CNContactStore.authorizationStatusForEntityType(CNEntityType.Contacts) == .Authorized;
        
        if status  {
			print("Permission to access contacts: Granted");
            fetchAllContacts()
        } else {
            AppDelegate.getAppDelegate().contactStore.requestAccessForEntityType(CNEntityType.Contacts) { (result, error) in
                status = result;
                if status {
                    self.fetchAllContacts();
                }
            }
        }
		
		let refresh = UIRefreshControl()
		refresh.addTarget(self, action: #selector(refreshTable), forControlEvents: .ValueChanged);
		tableView.addSubview(refresh);
        
        searchController.searchResultsUpdater = self;
        searchController.dimsBackgroundDuringPresentation = false;
        definesPresentationContext = true;
        tableView.tableHeaderView = searchController.searchBar;
    }
    
    override func viewWillAppear(animated: Bool) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func refreshTable(refreshController: UIRefreshControl) {
		let status = CNContactStore.authorizationStatusForEntityType(CNEntityType.Contacts) == .Authorized;
		if status {
			fetchAllContacts();
		}
		
		self.tableView.reloadData();
		refreshController.endRefreshing();
	}
    
    func fetchAllContacts() {
        let toFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey];
        let request = CNContactFetchRequest(keysToFetch: toFetch);
        request.sortOrder = .FamilyName;
		LocalContacts.removeAll();
		self.sections.removeAll();
        
        NSOperationQueue().addOperationWithBlock({
            do {
                try AppDelegate.getAppDelegate().contactStore.enumerateContactsWithFetchRequest(request, usingBlock: { (contact, result) in
                    if !LocalContacts.contains(contact) {
                        LocalContacts.append(contact);
                    }
                })
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.arrangeAndCreateTableIndexes()
                    self.tableView.reloadData();
                    print("Reloading table");
                })
                
            } catch let error {
                print(error)
            }
        })
    }
    
    func arrangeAndCreateTableIndexes() {
        
        var index = 0;
        print(LocalContacts.count);
        var contactsPhonenumbers : [String] = [];
		var contactNames : [String] = [];
        
        for i in (0 ..< LocalContacts.count) {
            
            
            // Sorting
            if LocalContacts[i].givenName + LocalContacts[i].familyName == "" {
                continue;
            }
            
            //let contact = contacts[i];
            let check = LocalContacts[i].familyName != "";
            let check2 = LocalContacts[index].familyName != "";
            
            var commonPrefix = "";
           // print("Current contact to check: \(contacts[i].givenName + " " + contacts[i].familyName)\nPrevious contact: \(contacts[index].givenName + " " + contacts[index].familyName)")
            
            if check && check2 {
                commonPrefix = LocalContacts[i].familyName.commonPrefixWithString(LocalContacts[index].familyName, options: .CaseInsensitiveSearch)
            } else if !check && check2 {
                commonPrefix = LocalContacts[i].givenName.commonPrefixWithString(LocalContacts[index].familyName, options: .CaseInsensitiveSearch)
            } else if check && !check2 {
                commonPrefix = LocalContacts[i].familyName.commonPrefixWithString(LocalContacts[index].givenName, options: .CaseInsensitiveSearch)
            } else {
                commonPrefix = LocalContacts[i].givenName.commonPrefixWithString(LocalContacts[index].givenName, options: .CaseInsensitiveSearch)
            }
            
            //print("Common prefix: " + commonPrefix);
            
            
            if (commonPrefix.characters.count == 0 ) {
                
                let string = LocalContacts[index].familyName != "" ? LocalContacts[index].familyName : LocalContacts[index].givenName //check ? contacts[index].familyName.uppercaseString : contacts[index].givenName.uppercaseString ;
                
                let firstCharacter = string[string.startIndex]
                let title = "\(firstCharacter)"
                let newSection = (index: index, length: i - index, title: title)
                sections.append(newSection)
                index = i;
            }
            
            
            //Checking for favorites
            if LocalContacts[i].isKeyAvailable(CNContactPhoneNumbersKey) {
                for phoneNumber: CNLabeledValue in LocalContacts[i].phoneNumbers {
                
                    let number = phoneNumber.value as! CNPhoneNumber
                    let numberString = number.stringValue;
					
                    contactsPhonenumbers.append(AppDelegate.getAppDelegate().stripPhoneNumber(numberString));
					contactNames.append(LocalContacts[i].givenName + " " +  LocalContacts[i].familyName);
                }
            }
			
			if (index + 1) == LocalContacts.count {
				if (commonPrefix.characters.count == 0 ) {
					
					let string = LocalContacts[index].familyName != "" ? LocalContacts[index].familyName : LocalContacts[index].givenName //check ? contacts[index].familyName.uppercaseString : contacts[index].givenName.uppercaseString ;
					
					let firstCharacter = string[string.startIndex]
					let title = "\(firstCharacter)"
					let newSection = (index: index, length: LocalContacts.count - index, title: title)
					sections.append(newSection)
					index = i;
				}
			}
        }
		
		RemoteData.checkForNumbers(contactsPhonenumbers, contactNames: contactNames) { () in }
    }
	
	
    func filterContactsSearch(searchText: String) {
		
        contactsSearch = LocalContacts.filter { contact in
            let fullname = contact.givenName.lowercaseString + contact.familyName.lowercaseString;
            return fullname.containsString(searchText.lowercaseString);
        }
        
        tableView.reloadData();
        
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContactsSearch(searchController.searchBar.text!);
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchController.active && searchController.searchBar.text != "" {
            return contactsSearch.count;
        }
        
        return sections[section].length;
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if searchController.active && searchController.searchBar.text != "" {
            return 1;
        }
        return sections.count;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if (searchController.active && searchController.searchBar.text != "") || sections.isEmpty {
            return "Results";
        }
        return sections[section].title;
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 32;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		//let cvc = CNContactViewController(forContact:  LocalContacts[sections[indexPath.section].index + indexPath.row]);
		
		//self.presentViewController(cvc, animated: true, completion: nil);
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let viewcell = tableView.dequeueReusableCellWithIdentifier("contactCell")! as UITableViewCell;
        if searchController.active && searchController.searchBar.text != "" {
            viewcell.textLabel?.text = contactsSearch[indexPath.row].givenName + " " + contactsSearch[indexPath.row].familyName;
            return viewcell;
        }
		
		let realIndex = sections[indexPath.section].index + indexPath.row;
		
		if !LocalContacts.isEmpty && !sections.isEmpty {
			
			viewcell.textLabel?.text = LocalContacts[realIndex].givenName + " " + LocalContacts[realIndex].familyName;
			let number = LocalContacts[realIndex].phoneNumbers
			viewcell.detailTextLabel?.text = "";
			viewcell.detailTextLabel?.textColor = UIColor.grayColor();
			
			if LocalUser.sharedInstance.userData.favorites != nil {
				if LocalUser.sharedInstance.userData.favorites?.isEmpty == false {
					for n in number {
						let num = n.value as! CNPhoneNumber
						let realNumber = num.stringValue;
						let x = AppDelegate.getAppDelegate().stripPhoneNumber(realNumber);
						
						let fav = LocalFavorites(name: nil, status: nil, smallPictureLink: nil, bigPictureLink: nil, phoneNumber: x, chosenLocale: nil, languageCode: nil, colorId: nil);
						
						if let index = LocalUser.sharedInstance.userData.favorites?.indexOf(fav) {
							viewcell.detailTextLabel?.text = LocalUser.sharedInstance.userData.favorites![index].status == nil ? "Hello, I'm using Render!" : LocalUser.sharedInstance.userData.favorites![index].status;
						}
					}
				}
			}
			
		}
		
        return viewcell;
    }
	
	
 
}