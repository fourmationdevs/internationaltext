//
//  RemoteData.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/7/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit

let PLAIN_ADDRESS = "staging.rendertranslate.com"//"192.168.0.16"
let SERVER_ADDRESS = "https://\(PLAIN_ADDRESS)/";
let API_ADDRESS = SERVER_ADDRESS + "api/";
let WEBSOCKET_ADDRESS = "wss://\(PLAIN_ADDRESS)/api/websocket"

public class RemoteData {
	
	static let ActivationCode = "11111";
	
	public class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
		let session = NSURLSession.sharedSession()
		
		let loadDataTask = session.dataTaskWithURL(url) { (data, response, error) -> Void in
			if let responseError = error {
				completion(data: nil, error: responseError)
			} else if let httpResponse = response as? NSHTTPURLResponse {
				if httpResponse.statusCode != 200 {
					let statusError = NSError(domain:"com.fourmation", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
					completion(data: nil, error: statusError)
				} else {
					completion(data: data, error: nil)
				}
			}
		}
		
		loadDataTask.resume()
	}
	
	public class func getMyUser(completion:(data: JSON) -> Void) {
		let session = NSURLSession.sharedSession();
		let param = ["userId" : LocalUser.sharedInstance.userData.userID!, "secretKey": LocalUser.sharedInstance.userData.secretKey!];
		let request = NSMutableURLRequest(URL: NSURL(string: API_ADDRESS + "get_user")!);
		request.HTTPMethod = "POST";
		request.addValue("application/json", forHTTPHeaderField: "Content-Type");
		request.addValue("application/json", forHTTPHeaderField: "Accept");
		
		do {
			try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(param, options: .PrettyPrinted);
			let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
				
				if error == nil {
					let json = JSON(data: data!);
					if json != nil {
						completion(data: json);
					}
				} else {
					print(error?.localizedDescription);
				}
			})
			task.resume();
			return;
		} catch let error {
			print(error);
		}
	}
	
	public class func sendVerificationCode(code: String, completion: (data: JSON) -> Void ) {
		
		let session = NSURLSession.sharedSession();
		let param = ["userId" : LocalUser.sharedInstance.userData.userID!, "activationCode" : code];
		//let JSONtoSend = JSON(dictionaryLiteral: ("phoneNumber", number));
		
		let request = NSMutableURLRequest(URL: NSURL(string: API_ADDRESS + "validate_user")!);
		request.HTTPMethod = "POST"
		request.addValue("application/json", forHTTPHeaderField: "Content-Type");
		request.addValue("application/json", forHTTPHeaderField: "Accept");
		
		do {
			try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(param, options: .PrettyPrinted);
			
			
			let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
				
				//print(response?.description);
				if error == nil {
					let json = JSON(data: data!);
					print(json);
					
					if json["success"].bool != nil {
						completion(data: json);
					}
					
				} else {
					print(error?.localizedDescription);
				}
			})
			task.resume();
			return;
			
		} catch let error {
			print(error);
		}
	}
	
	
	public class func sendUserData(completion: (data: JSON) -> Void) {
		let session = NSURLSession.sharedSession();
		var param : Dictionary<String, AnyObject> = ["userId" : LocalUser.sharedInstance.userData.userID!,
		                                             "secretKey" : LocalUser.sharedInstance.userData.secretKey!,
		                                             "displayName" : LocalUser.sharedInstance.userData.name!,
		                                             "defaultLanguage" : LocalUser.sharedInstance.userData.languageCode!,
		                                             "platform" : "ios", "locale" : LocalUser.sharedInstance.userData.chosenLocale!,
		                                             "color" : LocalUser.sharedInstance.userData.colorThemeID!];
		
		if LocalUser.sharedInstance.userData.statusMessage != nil {
			param["statusMessage"] = LocalUser.sharedInstance.userData.statusMessage!;
		}
		
		//let JSONtoSend = JSON(dictionaryLiteral: ("phoneNumber", number));
		
		let request = NSMutableURLRequest(URL: NSURL(string: API_ADDRESS + "update_user")!);
		request.HTTPMethod = "POST"
		request.addValue("application/json", forHTTPHeaderField: "Content-Type");
		request.addValue("application/json", forHTTPHeaderField: "Accept");
		
		do {
			try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(param, options: .PrettyPrinted);
			let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
				//print(response?.description);
				if error == nil {
					let json = JSON(data: data!);
					print(json);
					
					
					if json["success"] == true  {
						completion(data: json);
					}
					
				} else {
					print(error?.localizedDescription);
				}
			})
			task.resume();
			return;
		} catch let error {
			print(error);
		}
		
	}
	
	public class func sendPhoneNumber(number: String, completion: (data: JSON) -> Void) {
		
		let session = NSURLSession.sharedSession();
		let param = ["phoneNumber" : number];
		//let JSONtoSend = JSON(dictionaryLiteral: ("phoneNumber", number));
		
		let request = NSMutableURLRequest(URL: NSURL(string: API_ADDRESS + "create_user")!);
		request.HTTPMethod = "POST"
		request.addValue("application/json", forHTTPHeaderField: "Content-Type");
		request.addValue("application/json", forHTTPHeaderField: "Accept");
		
		do {
			try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(param, options: .PrettyPrinted);
			
			
			let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
				
				//print(response?.description);
				if error == nil {
					let json = JSON(data: data!);
					print(json);
					
					if json != nil {
						if json["success"].bool != nil || json["exists"] == true {
							completion(data: json);
						}
					}
					
				} else {
					print(error?.localizedDescription);
				}
			})
			task.resume();
			return;
			
		} catch let error {
			print(error);
		}
	}
	
	public class func checkNumber(number: String, completion: (user: LocalFavorites) -> Void) {
		let session = NSURLSession.sharedSession();
		let param = ["userId" : LocalUser.sharedInstance.userData.userID!,
		             "secretKey" : LocalUser.sharedInstance.userData.secretKey!,
		             "phoneNumbers": [number]];
		//let JSONtoSend = JSON(dictionaryLiteral: ("phoneNumber", number));
		
		let request = NSMutableURLRequest(URL: NSURL(string: API_ADDRESS + "check_numbers")!);
		request.HTTPMethod = "POST"
		request.addValue("application/json", forHTTPHeaderField: "Content-Type");
		request.addValue("application/json", forHTTPHeaderField: "Accept");
		
		do {
			try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(param, options: .PrettyPrinted);
			let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
				if error == nil {
					let json = JSON(data: data!);
					if json["success"] == true {
						var user : LocalFavorites!
						if let users = json["users"].array?[0] {
							print(users);
							let phone = users["phoneNumber"].string;
							let name = users["displayName"].string;
							let sPicture = users["smallProfilePicture"].string?.stringByReplacingOccurrencesOfString("\\", withString: "");
							let bPicture = users["bigProfilePicture"].string?.stringByReplacingOccurrencesOfString("\\", withString: "");
							let status = users["statusMessage"].string;
							let code = users["defaultLanguage"].string;
							let locale = users["locale"].string;
							let color = users["color"].int;
							
							user = LocalFavorites(name: name, status: status, smallPictureLink: sPicture, bigPictureLink: bPicture, phoneNumber: phone, chosenLocale: locale, languageCode: code, colorId: color);
						}
						completion(user: user);
					}
				} else {
					print(error?.localizedDescription);
				}
			})
			task.resume();
			return;
			
		} catch let error {
			print(error);
		}
	}
	
	public class func checkForNumbers(numbers: [String], contactNames: [String], completion: (Void) -> Void) {
		
		let session = NSURLSession.sharedSession();
		let param = ["userId" : LocalUser.sharedInstance.userData.userID!,
		             "secretKey" : LocalUser.sharedInstance.userData.secretKey!,
		             "phoneNumbers": numbers];
		//let JSONtoSend = JSON(dictionaryLiteral: ("phoneNumber", number));
		
		let request = NSMutableURLRequest(URL: NSURL(string: API_ADDRESS + "check_numbers")!);
		request.HTTPMethod = "POST"
		request.addValue("application/json", forHTTPHeaderField: "Content-Type");
		request.addValue("application/json", forHTTPHeaderField: "Accept");
		
		do {
			try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(param, options: .PrettyPrinted);
			
			
			let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
				
				//print(response?.description);
				if error == nil {
					let json = JSON(data: data!);
					//print(json);
					
					
					if json["success"] == true {
						
						LocalUser.sharedInstance.userData.favorites = [];
						
						for users in json["users"].array! {
							print(users);
							let phone = users["phoneNumber"].string;
							var name = users["displayName"].string;
							let sPicture = users["smallProfilePicture"].string?.stringByReplacingOccurrencesOfString("\\", withString: "");
							let bPicture = users["bigProfilePicture"].string?.stringByReplacingOccurrencesOfString("\\", withString: "");
							let status = users["statusMessage"].string;
							let code = users["defaultLanguage"].string;
							let locale = users["locale"].string;
							let color = users["color"].int;
							
							for (index, number) in numbers.enumerate() {
								if number == phone {
									name = contactNames[index];
								}
							}
							
							let fav = LocalFavorites(name: name, status: status, smallPictureLink: sPicture, bigPictureLink: bPicture, phoneNumber: phone, chosenLocale: locale, languageCode: code, colorId: color);
							//let favs = LocalUser.sharedInstance.userData.favorites
							//print(favs);
							
							if (LocalUser.sharedInstance.userData.favorites != nil) {
								
								let i = LocalUser.sharedInstance.userData.favorites!.indexOf(fav);
								
								if i != nil {
									LocalUser.sharedInstance.userData.favorites![i!] = fav;
								} else {
									LocalUser.sharedInstance.userData.favorites!.append(fav)
								}
							} else {
								LocalUser.sharedInstance.userData.favorites = [fav];
							}
						}
						
						LocalUser.sharedInstance.saveUser();
					}
					
				} else {
					print(error?.localizedDescription);
				}
			})
			task.resume();
			return;
			
		} catch let error {
			print(error);
		}
	}
	
	class func requestAndPullAvailableLanguages(success: (data: NSData!) -> Void ) {
		
		loadDataFromURL(NSURL(string: API_ADDRESS + "get_available_languages?locale=" + NSLocale.preferredLanguages()[0])!) { (data, error) in
			
			if let resultData = data {
				success(data: resultData);
			} else {
				print(error?.localizedDescription);
			}
		}
		
		
	}
	
	public class func requestPicture(url: String, completion: (data: NSData) -> Void) {
		
		let session = NSURLSession.sharedSession()
		
		let loadDataTask = session.dataTaskWithURL(NSURL(string: SERVER_ADDRESS + url)!) { (data, response, error) -> Void in
			if let responseError = error {
				print(responseError.description);
				//completion(data: nil, error: responseError)
			} else if let httpResponse = response as? NSHTTPURLResponse {
				if httpResponse.statusCode != 200 {
					let statusError = NSError(domain:"com.fourmation", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
					print(statusError);
					//completion(data: nil, error: statusError)
				} else {
					dispatch_async(dispatch_get_main_queue(), {
						completion(data: data!);
					})
				}
			}
		}
		loadDataTask.resume()
		
		
	}
	
	
	public class func uploadPicture() {
		
		let url = API_ADDRESS + "update_user?userId=\(LocalUser.sharedInstance.userData.userID!)&secretKey=\(LocalUser.sharedInstance.userData.secretKey!)";
		let request = NSMutableURLRequest(URL: NSURL(string: url)!);
		request.HTTPMethod = "POST";
		
		let boundary = generateBoundaryString();
		request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type");
		
		let image_data = LocalUser.sharedInstance.userData.profilePic;
		
		if image_data == nil {
			return;
		}
		
		let body = NSMutableData();
		let fname = LocalUser.sharedInstance.userData.phoneNumber! + ".jpg";
		let mimeType = "image/jpg"
		/*
		body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		body.appendData("Content-Disposition:form-data; name=\"userId\"\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		body.appendData("\(LocalUser.sharedInstance.userData.userID!)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		
		body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		body.appendData("Content-Disposition:form-data; name=\"secretKey\"\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		body.appendData("\(LocalUser.sharedInstance.userData.secretKey!)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		*/
		
		// WATCH OUT FOR THE -- THEY MEAN THE END OF A FILE IF IT COMES AFTER SETTING A BOUNDARY // EX: --\(BOUNDARY)--\r\n
		
		body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		body.appendData("Content-Disposition:form-data; name=\"smallProfilePicture\"; filename=\"\(fname)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
		body.appendData("Content-Type: \(mimeType)\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
		body.appendData(image_data!);
		body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		//body.appendData("--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		
		
		body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		body.appendData("Content-Disposition:form-data; name=\"bigProfilePicture\"; filename=\"\(fname)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
		body.appendData("Content-Type: \(mimeType)\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
		body.appendData(image_data!);
		body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		body.appendData("--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!);
		//print(body.description);
		
		request.HTTPBody = body;
		
		let session = NSURLSession.sharedSession()
		
		
		let task = session.dataTaskWithRequest(request) {
			(
			let data, let response, let error) in
			
			guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
				print("error")
				return
			}
			
			let dataJson = JSON(data: data!);
			
			let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
			print(dataString)
			
			let bool = dataJson["success"].bool;
			
			if bool != nil {
				if bool! {
					LocalUser.sharedInstance.userData.smallPictureLink = dataJson["user"]["smallPictureLink"].string?.stringByReplacingOccurrencesOfString("\\", withString: "");
					LocalUser.sharedInstance.userData.bigPictureLink = dataJson["user"]["bigPictureLink"].string?.stringByReplacingOccurrencesOfString("\\", withString: "");
					LocalUser.sharedInstance.saveUser();
				}
			}
			
			
			
			
			
		}
		
		task.resume()
		
		
		
	}
	
	class func generateBoundaryString() -> String
	{
		return "Boundary-\(NSUUID().UUIDString)"
	}
	
}