//
//  VerificationViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/14/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import Spring

class VerificationViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var viewContainer: SpringView!
    @IBOutlet weak var verificationCodeLabel: UILabel!
    @IBOutlet weak var codeTextfield: UITextField!
    @IBOutlet weak var confirmButton: UIButton!
	@IBOutlet weak var errorMessage: SpringLabel!
    
    var phoneNumber = LocalUser.sharedInstance.userData.phoneNumber
    
    @IBAction func confirmButtonTap(sender: UIButton) {
        
        let string : String
        if codeTextfield.text?.characters.count == 5 {
            string = codeTextfield.text!;
            print(string);
			
			//self.errorMessage.hidden = true;
			
			if errorMessage.alpha != 0.0 {
				self.errorMessage.animation = "fadeOut"
				self.errorMessage.curve = "easeIn";
				self.errorMessage.delay = 0.0;
				self.errorMessage.animateToNext {
					//self.errorMessage.hidden = true
				}
			}
			   
            RemoteData.sendVerificationCode(string) { (data) in
				
				//Authenticated
				if data["success"] == true {
					print(data["secretKey"].string);
					LocalUser.sharedInstance.userData.secretKey = data["secretKey"].string;
					LocalUser.sharedInstance.saveUser();
					dispatch_async(dispatch_get_main_queue(), {
						self.performSegueWithIdentifier("validationToAccountSegue", sender: self);
					})
				} else {
					//self.errorMessage.hidden = false;
					dispatch_async(dispatch_get_main_queue(), { 
						self.errorMessage.alpha = 1;
						self.errorMessage.animation = "squeezeUp"
						self.errorMessage.curve = "easeIn";
						self.errorMessage.delay = 0.1;
						self.errorMessage.animate();
					})
				}
            }
        }
    }
    
    var isUp = false;
    
    
    override func viewDidLoad() {
        //confirmButton.layer.borderWidth = 2;
        
        confirmButton.layer.cornerRadius = confirmButton.frame.height * 0.20;
        viewContainer.layer.cornerRadius = viewContainer.frame.height * 0.05;
        viewContainer.layer.borderWidth = 0;
        verificationCodeLabel.text = "A verification code has been sent to \(phoneNumber!)."
        
        codeTextfield.delegate = self;
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil);
        
		errorMessage.layer.cornerRadius = errorMessage.frame.height * 0.25;
		
    }
    
    override func viewDidAppear(animated: Bool) {
        //UIApplication.sharedApplication().sendAction(<#T##action: Selector##Selector#>, to: <#T##AnyObject?#>, from: <#T##AnyObject?#>, forEvent: <#T##UIEvent?#>)
        viewContainer.animate();
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            
            if isUp == false {
                self.viewContainer.frame.origin.y -= keyboardSize.height * 0.5
                isUp = true;
            }
            
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() {
            
            //if isUp {
            isUp = false;
            self.viewContainer.frame.origin.y += keyboardSize.height * 0.5
           // }
        }
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let count = textField.text?.characters.count;
        let unicode = string.unicodeScalars;

        
        let index = unicode[unicode.startIndex].value;
        let char = string.cStringUsingEncoding(NSUTF8StringEncoding)!;
        let backspace = strcmp(char, "\\b");
        
        if backspace == -92 {
            return true;
        }
        
        
        
        if count >= 5 {
            return false;
        }
        
        if (index >= 48 && index <= 57) {
            return true;
        }
        
        return false;
    }
    
    
    
    
    
    
    
    
}
