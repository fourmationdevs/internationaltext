//
//  TabBarViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/6/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class TabBarViewController: UITabBarController, CNContactPickerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //let contactPickerVC = CNContactPickerViewController();
        //contactPickerVC.delegate = self;
        //viewControllers?.append(contactPickerVC);
        
        //presentViewController(contactPickerVC, animated: true, completion: nil);
        SocketHandler.sharedInstance;
		
		tabBar.tintColor = ColorManager.sharedInstance.currentThemeColor;
		
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
