//
//  LocalStorage.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/17/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import Foundation
import Contacts
import SQLite

var LocalContacts = [CNContact]();



class DatabaseManager {
	static let instance = DatabaseManager();
	
	var db : Connection!
	let messages = Table("messages");
	let id = Expression<Int64>("id");
	let chatId = Expression<String>("chat_id");
	let translatedText = Expression<String>("translated_text");
	let translatedCode = Expression<String>("translated_code");
	let originalText = Expression<String>("original_text");
	let originalCode = Expression<String>("original_code");
	let date = Expression<NSDate>("date");
	let senderId = Expression<String>("sender_id");
	let senderNumber = Expression<String>("sender_number")
	
	private init() {
		do {
			let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first;
			db = try Connection("\(path!)/messageDB.sqlite3");
			
			try db.run(messages.create(ifNotExists: true) { t in
				t.column(id, primaryKey: true);
				t.column(chatId);
				t.column(translatedText);
				t.column(translatedCode);
				t.column(originalText);
				t.column(originalCode);
				t.column(date);
				t.column(senderId);
				t.column(senderNumber);
				})
			
			print("Database initialized correctly");
		} catch let error {
			print(error);
		}
	}
	
	func insertMessage(message: Message, chatId: String ) {
		do {
			try db.run(messages.insert(self.chatId <- chatId, translatedText <- message.text!, translatedCode <- message.transTag, originalText <- message.ogText, originalCode <- message.ogTag, date <- message.messageDate, senderId <- message.senderID!, senderNumber <- message.senderNumber));
			
			displayEntries();
		} catch let error {
			print(error);
		}
	}
	
	func displayEntries() {
		
		do {
			for msg in try db.prepare(messages) {
				print("id: \(msg[id]), chatId: \(msg[chatId]), text: \(msg[translatedText]), date: \(msg[date])");
			}
		} catch let error {
			print(error);
		}
	}
	
	func messagesForChatId(chatId: String, count: Int, fromIndex: Int) -> [Message] {
		var msgs : [Message] = []
		
		do {
			let query = messages.filter(self.chatId == chatId).order(self.date.desc).limit(count, offset: fromIndex);
			
			for msgRow : Row in try db.prepare(query) {
				print("id: \(msgRow[id]), senderName: \(senderNumber), chatId: \(msgRow[self.chatId]), text: \(msgRow[translatedText]), date: \(msgRow[date])");
				let newMsg = entryToMessage(msgRow);
				msgs.append(newMsg);
			}
			
		} catch let error {
			print(error);
		}
		
		return msgs.reverse();
	}
	
	func lastMessageFromChat(chatId: String?) -> Message? {
		if chatId == nil {
			return nil;
		}
		
		let query = messages.filter(self.chatId == chatId!).order(self.date.desc).limit(1, offset: 0);
		let msg = db.pluck(query);
		if msg != nil {
			return entryToMessage(msg!);
		}
		
		return nil;
		
	}
	
	func deleteAllMessages() {
		do {
			try db.run(messages.delete())
		} catch let error {
			print(error);
		}
		
	}
	
	private func entryToMessage(msgRow: Row) -> Message {
		let newMsg = Message(text: msgRow[translatedText], senderID: msgRow[senderId], messageDate: msgRow[date], senderImage: nil);
		newMsg.ogText = msgRow[originalText];
		newMsg.ogTag = msgRow[originalCode];
		newMsg.transTag = msgRow[translatedCode];
		newMsg.senderNumber = msgRow[senderNumber];
		return newMsg;
	}
}


class LocalChats : NSObject, NSCoding {
	
	
	var participants : [String]?
	var favoritesInChat : [LocalFavorites]?
	//var messages : [LocalMessages]?
	var lastSentMessageDate : String?
	var languages : String?
	var chatID : String?
	
	
	struct ChatKeys {
		static let participantsKey = "participants";
		static let messagesKey = "messages";
		static let languagesKey = "languages";
		static let dateKey = "lastMessageDate";
		static let favoritesInChat = "pNames";
		static let chatIdKey = "chatId"
	}
	
	init(participants: [String]?, favoritesInChat: [LocalFavorites]?, lastSentMessage: String?, languages: String?, chatID : String?) {
		super.init();
		
		self.participants = participants;
		self.favoritesInChat = favoritesInChat == nil ? [] : favoritesInChat;
		//self.messages = messages;
		self.lastSentMessageDate = lastSentMessage;
		self.languages = languages;
		
		if participants != nil && (self.favoritesInChat?.isEmpty)! {
			self.checkFavorites();
		}
		
		if participants != nil && chatID == nil {
			self.chatID = "";
			self.participants?.sortInPlace { $0 < $1 };
			
			for p in participants! {
				self.chatID! += p;
			}
			
		} else {
			self.chatID = chatID;
		}
	}
	
	
	
	required convenience init?(coder aDecoder: NSCoder) {
		let participants = aDecoder.decodeObjectForKey(ChatKeys.participantsKey) as? [String];
		//		let messages = aDecoder.decodeObjectForKey(ChatKeys.messagesKey) as? [LocalMessages];
		let messageDate = aDecoder.decodeObjectForKey(ChatKeys.dateKey) as? String;
		let languages = aDecoder.decodeObjectForKey(ChatKeys.languagesKey) as? String;
		let pNames = aDecoder.decodeObjectForKey(ChatKeys.favoritesInChat) as? [LocalFavorites];
		let chatId = aDecoder.decodeObjectForKey(ChatKeys.chatIdKey) as? String;
		self.init(participants: participants, favoritesInChat: pNames, lastSentMessage: messageDate, languages: languages, chatID: chatId);
	}
	
	func encodeWithCoder(aCoder: NSCoder) {
		aCoder.encodeObject(participants, forKey: ChatKeys.participantsKey);
		//aCoder.encodeObject(messages, forKey: ChatKeys.messagesKey);
		aCoder.encodeObject(languages, forKey: ChatKeys.languagesKey);
		aCoder.encodeObject(lastSentMessageDate, forKey: ChatKeys.dateKey);
		aCoder.encodeObject(favoritesInChat, forKey: ChatKeys.favoritesInChat);
		aCoder.encodeObject(chatID, forKey: ChatKeys.chatIdKey);
	}
	
	func checkFavorites() {
		
		if LocalUser.sharedInstance.userData.favorites != nil {
			
			for number in participants! {
				let dummy = LocalFavorites(name: nil, status: nil, smallPictureLink: nil, bigPictureLink: nil, phoneNumber: number, chosenLocale: nil, languageCode: nil, colorId: nil);
				let index = LocalUser.sharedInstance.userData.favorites?.indexOf(dummy);
				if index != nil {
					favoritesInChat?.append(LocalUser.sharedInstance.userData.favorites![index!]);
				} else {
					RemoteData.checkNumber(number, completion: { (user) in
						self.favoritesInChat?.append(user);
						LocalUser.sharedInstance.saveUser();
					})
				}
				
			}
			
			
			/*
			for fav in LocalUser.sharedInstance.userData.favorites! {
				for number in participants! {
					if number == fav.phoneNumber! {
						favoritesInChat?.append(fav);
					}
				}
			}
			*/
		}
	}
	
	override func isEqual(object: AnyObject?) -> Bool {
		
		//var isTrue = false;
		let otherChat = object as? LocalChats
		
		if otherChat == nil {return false};
		
		return self.chatID! == otherChat?.chatID!;
	}
	
	
	func chatTitle() -> String {
		
		if favoritesInChat == nil {
			return "";
		}
		
		if favoritesInChat!.isEmpty {
			return "";
		} else {
			var title = "";
			for (index, p) in favoritesInChat!.enumerate() {
				title += "\(p.name!)";
				title += ", "
			}
			
			if title.length > 2 {
				title.removeAtIndex(title.endIndex.predecessor());
				title.removeAtIndex(title.endIndex.predecessor());
			}
			
			return title;
		}
	}
	
	func isGroup() -> Bool {
		if participants == nil {
			return false;
		} else if participants!.count == 1 {
			return false;
		} else {
			return true;
		}
		
	}
}

/*
class LocalMessages : NSObject, NSCoding {


var translatedText : String?
var originalText : String?
var translatedCode : String?
var originalCode : String?
var messageType : String? // 0 - Own, 1 - Incoming
var dateSent : String?


struct MessageKeys {
static let tTextKey = "translated";
static let oTextKey = "original";
static let tCodeKey = "transcode";
static let oCodeKey = "ogcode";
static let dateKey = "dateSent";
static let messageType = "messageType"
}

init(tText: String?, ogText : String?, tTextCode: String?, ogCode: String?, dateSent: String?, messageType: String?) {
translatedText		= tText;
originalText		= ogText;
translatedCode		= tTextCode;
originalCode		= ogCode;
self.dateSent		= dateSent;
self.messageType	= messageType;
}

init(message: Message) {
translatedText	= message.text;
translatedCode	= message.transTag;
originalText	= message.ogText;
originalCode	= message.ogTag;
messageType		= message.senderID;

let formatter = NSDateFormatter();
formatter.dateStyle = .ShortStyle;
formatter.timeStyle = .ShortStyle;
let string = formatter.stringFromDate(message.messageDate);
dateSent = string;
}



required convenience init?(coder aDecoder: NSCoder) {
let tText		= aDecoder.decodeObjectForKey(MessageKeys.tTextKey) as? String;
let ogText		= aDecoder.decodeObjectForKey(MessageKeys.oTextKey) as? String;
let tCode		= aDecoder.decodeObjectForKey(MessageKeys.tCodeKey) as? String;
let ogCode		= aDecoder.decodeObjectForKey(MessageKeys.oCodeKey) as? String;
let dateSent	= aDecoder.decodeObjectForKey(MessageKeys.dateKey) as? String;
let type		= aDecoder.decodeObjectForKey(MessageKeys.messageType) as? String;

self.init(tText: tText, ogText: ogText, tTextCode: tCode, ogCode: ogCode, dateSent: dateSent, messageType: type);
}

func encodeWithCoder(aCoder: NSCoder) {
aCoder.encodeObject(translatedText, forKey: MessageKeys.tTextKey);
aCoder.encodeObject(originalText, forKey: MessageKeys.oTextKey);
aCoder.encodeObject(translatedCode, forKey: MessageKeys.tCodeKey);
aCoder.encodeObject(originalCode, forKey: MessageKeys.oCodeKey);
aCoder.encodeObject(dateSent, forKey: MessageKeys.dateKey);
aCoder.encodeObject(messageType, forKey: MessageKeys.messageType);
}

override func isEqual(object: AnyObject?) -> Bool {

if self.dateSent == nil || self.dateSent == "" {
return false;
}


if let other = object as? LocalMessages {
if other.dateSent == nil || other.dateSent == "" {
return false;
}

if other.dateSent == self.dateSent {
return true;
}
}
return false;
}

func toJLMessage() -> Message {

let datePicker = NSDateFormatter();
datePicker.dateStyle = .ShortStyle;
datePicker.timeStyle = .ShortStyle;
let date = datePicker.dateFromString(dateSent!);

let msg = Message(text: translatedText!, senderID: messageType!, messageDate: date!, senderImage: nil);
msg.ogTag = originalCode != nil ? originalCode! : "";
msg.ogText = originalText != nil ? originalText! : "";
msg.transTag = translatedCode != nil ? translatedCode! : "";

return msg;
}



}
*/


