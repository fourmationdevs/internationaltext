//
//  LocalFavorites.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/16/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import Foundation



public class LocalFavorites : NSObject, NSCoding {
    
    
    struct FavoritesKeys {
        static let name = "name";
        static let status = "status";
        static let smallPictureLink = "smallPicture";
        static let bigPictureLink = "bigPicture"
        static let phoneNumber = "phone";
        static let messages = "messages";
        static let profileImage = "imageData";
		static let chosenLocale = "favLocale";
		static let languageCode = "langCode"
		static let colorId = "favColorId"
    }
    
    
    var name : String?
    var status : String?
    var smallPictureLink : String?
    var bigPictureLink : String?
    var profileImageData : NSData?
    var phoneNumber : String?
	var chosenLocale : String?
	var languageCode : String?;
	var colorId : Int?
    
    //var messages : [LocalMessages]?
    
	init(name: String?, status: String?, smallPictureLink: String?, bigPictureLink: String?, phoneNumber: String?, chosenLocale: String?, languageCode: String?, colorId: Int?) {
        self.name = name;
        self.status = status;
        self.smallPictureLink = smallPictureLink;
        self.bigPictureLink = bigPictureLink;
        self.phoneNumber = phoneNumber;
		self.chosenLocale = chosenLocale;
		self.languageCode = languageCode;
//        self.messages = messages;
		self.colorId = colorId == nil ? 12 : colorId;
    }
    
    required convenience public init?(coder aDecoder: NSCoder) {
        let name				= aDecoder.decodeObjectForKey(FavoritesKeys.name) as? String;
        let status				= aDecoder.decodeObjectForKey(FavoritesKeys.status) as? String;
        let smallPictureLink	= aDecoder.decodeObjectForKey(FavoritesKeys.smallPictureLink) as? String;
        let bigPictureLink		= aDecoder.decodeObjectForKey(FavoritesKeys.bigPictureLink) as? String
        let phone				= aDecoder.decodeObjectForKey(FavoritesKeys.phoneNumber) as? String;
//        let messages			= aDecoder.decodeObjectForKey(FavoritesKeys.messages) as? [LocalMessages];
		let locale				= aDecoder.decodeObjectForKey(FavoritesKeys.chosenLocale) as? String;
		let code				= aDecoder.decodeObjectForKey(FavoritesKeys.languageCode) as? String;
		let color				= aDecoder.decodeObjectForKey(FavoritesKeys.colorId) as? Int;
		
		self.init(name: name, status: status, smallPictureLink: smallPictureLink, bigPictureLink: bigPictureLink, phoneNumber: phone, chosenLocale: locale, languageCode: code, colorId: color);
        
        //profileImageData = aDecoder.decodeObjectForKey(FavoritesKeys.profileImage) as? NSData;
    }
    
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: FavoritesKeys.name);
        aCoder.encodeObject(status, forKey: FavoritesKeys.status);
        aCoder.encodeObject(smallPictureLink, forKey: FavoritesKeys.smallPictureLink);
        aCoder.encodeObject(phoneNumber, forKey: FavoritesKeys.phoneNumber);
//        aCoder.encodeObject(messages, forKey: FavoritesKeys.messages);
        aCoder.encodeObject(bigPictureLink, forKey: FavoritesKeys.bigPictureLink);
		aCoder.encodeObject(chosenLocale, forKey: FavoritesKeys.chosenLocale);
		aCoder.encodeObject(languageCode, forKey: FavoritesKeys.languageCode);
		aCoder.encodeObject(colorId, forKey: FavoritesKeys.colorId);
        //aCoder.encodeObject(profileImageData, forKey:  FavoritesKeys.profileImage);
    }
    
    override public func isEqual(object: AnyObject?) -> Bool {
        if self.phoneNumber == nil{ return false; }
        if let fav = object as? LocalFavorites {
            if fav.phoneNumber == nil { return false }
            if fav.phoneNumber == self.phoneNumber { return true; }
        }
        return false;
    }
    
	override public var description: String {
		
		return "\(name) - \(phoneNumber)"
		
	}
		
}