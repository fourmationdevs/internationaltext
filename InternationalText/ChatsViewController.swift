 //
//  ChatsViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/6/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
import JLChatViewController

class ChatsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
	
	var chats : [LocalChats]? = LocalUser.sharedInstance.userData.chats == nil ? [] : LocalUser.sharedInstance.userData.chats;
	@IBOutlet weak var chatTableView: UITableView!
	
	
	//Search
	let searchController = UISearchController(searchResultsController: nil);
	var searchChats : [LocalChats]? = [];
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Pull all chats the user currently has with his contacts
		// Assemble the table
		JLBundleController.loadJLChatStoryboard();
		
		chatTableView.delegate = self;
		chatTableView.dataSource = self;
		
		let nib = UINib(nibName: "ChatTableViewCell", bundle: nil);
		chatTableView.registerNib(nib, forCellReuseIdentifier: "chatTableCell");
		chatTableView.registerNib(UINib(nibName: "ChatHeaderViewCell", bundle: nil), forCellReuseIdentifier: "chatHeaderCell");
		
		
		searchController.searchResultsUpdater = self;
		searchController.dimsBackgroundDuringPresentation = false;
		definesPresentationContext = true;
		chatTableView.tableHeaderView = searchController.searchBar;
		
		
	}
	
	override func viewWillAppear(animated: Bool) {
		//self.chatTableView.reloadData();
		print("Entering Chat Window with \(LocalUser.sharedInstance.userData.chats?.count) chats stored in LocalUser");

		if LocalUser.sharedInstance.userData.chats != nil {
			chats?.removeAll();
			chats = LocalUser.sharedInstance.userData.chats!;
			
			self.chatTableView.reloadData();
		}

	}
	
	override func viewWillDisappear(animated: Bool) {
		LocalUser.sharedInstance.saveUser();
	}
	
	
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func filterAndSearchChats(searchText: String) {
		searchChats = chats!.filter { chat in
			var fullname : String!
			
			for contact in LocalContacts {
				for number in contact.phoneNumbers {
					if number == chat.participants![0].lowercaseString {
						fullname = contact.givenName + " " + contact.familyName;
					} else {
						fullname = chat.participants![0].lowercaseString;
					}
				}
			}
			
			return fullname.containsString(searchText.lowercaseString);
		}
		
		chatTableView.reloadData();
	}
	
	func updateSearchResultsForSearchController(searchController: UISearchController) {
		filterAndSearchChats(searchController.searchBar.text!);
	}
	
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		if indexPath.row == 0 && !searchController.active && searchController.searchBar.text == "" {
			return 36
		}
		
		return 64;
	}
	
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		let viewCell = tableView.dequeueReusableCellWithIdentifier("chatTableCell") as! ChatTableViewCell;
		
		if(indexPath.row == 0 && !searchController.active && searchController.searchBar.text == "") {
			let header = tableView.dequeueReusableCellWithIdentifier("chatHeaderCell") as! ChatHeaderViewCell;
			return header;
		}

		///If Searching ////////////////////////////////////////////////////////////
		if searchController.active && searchController.searchBar.text != "" {
			if (searchChats!.count > 0) {
				let lastMessage = DatabaseManager.instance.lastMessageFromChat(searchChats![indexPath.row].chatID);
				let formatter = NSDateFormatter()
				formatter.dateStyle = .MediumStyle
				formatter.timeStyle = .ShortStyle;
				formatter.doesRelativeDateFormatting = true;
				
				if lastMessage != nil {
					viewCell.chLastMessage.text = lastMessage?.text;
					viewCell.chTimeLastMesssage.text = formatter.stringFromDate((lastMessage?.messageDate)!);
				}
				
				viewCell.chStatusLastMessage.text = searchChats![indexPath.row].languages;
				
				if searchChats![indexPath.row].favoritesInChat != nil {
					viewCell.chFriendName.text = searchChats![indexPath.row].chatTitle();
					
					if let link = searchChats![indexPath.row].favoritesInChat![0].smallPictureLink {
						viewCell.chFriendAvatar.kf_setImageWithURL(NSURL(string: link)!);
					} else {
						let image = UIImage(named: "medal-50");
						viewCell.chFriendAvatar.image = image;
					}
				} else {
					let image = UIImage(named: "medal-50");
					viewCell.chFriendAvatar.image = image;
				}
			}
			return viewCell;
		}
		////////////////////////////////////////////////////////////////////////////
		

		
		///////////// Regular Chat Cell //////////////////////////
		let realIndex = indexPath.row - 1;
		if realIndex >= chats?.count || chats == nil {
			return viewCell;
		}
		
		if chats?[realIndex] != nil {
			let lastMessage = DatabaseManager.instance.lastMessageFromChat(chats![realIndex].chatID);
			let formatter = NSDateFormatter()
			formatter.dateStyle = .MediumStyle
			formatter.timeStyle = .ShortStyle;
			formatter.doesRelativeDateFormatting = true;
			
			if lastMessage != nil {
				viewCell.chLastMessage.text =  lastMessage?.text;
				viewCell.chTimeLastMesssage.text = formatter.stringFromDate((lastMessage?.messageDate)!);
			}

			viewCell.chStatusLastMessage.text = chats![realIndex].languages;
			
			if chats![realIndex].favoritesInChat != nil {
				viewCell.chFriendName.text = chats![realIndex].chatTitle();
				
				if chats![realIndex].favoritesInChat!.isEmpty {
					return viewCell;
				}
				
				if chats![realIndex].favoritesInChat?[0].colorId != nil {
					viewCell.chFriendAvatar.layer.borderColor = chats![realIndex].isGroup() ? ColorManager.sharedInstance.currentThemeColor.CGColor : ColorManager.sharedInstance.getColorforID(chats![realIndex].favoritesInChat![0].colorId!).CGColor;
				} else {
					viewCell.chFriendAvatar.layer.borderColor = chats![realIndex].isGroup() ? ColorManager.sharedInstance.currentThemeColor.CGColor : ColorManager.sharedInstance.getColorforID(12).CGColor;
				}
				
				
				if let link = chats![realIndex].favoritesInChat![0].smallPictureLink {
					viewCell.chFriendAvatar.kf_setImageWithURL(NSURL(string: link)!);
				} else {
					let image = UIImage(named: "medal-50");
					viewCell.chFriendAvatar.image = image;
				}
			} else {
				let image = UIImage(named: "medal-50");
				viewCell.chFriendAvatar.image = image;
			}
		}
		

		return viewCell;
		/////////////////////////////////////////////////////////
		
		
	}
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		let numOfSections = 1;
		
		/*
		if (chats!.count > 0)
		{
			tableView.separatorStyle = .SingleLine;
			numOfSections                 = 1;
			tableView.backgroundView   = nil;
		}
		else
		{
			let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
			noDataLabel.text = "Let's start chatting!";
			noDataLabel.textColor = UIColor.blackColor();
			noDataLabel.textAlignment = .Center;
			tableView.backgroundView = noDataLabel;
			tableView.separatorStyle = .None;
		}
		*/
		
		return numOfSections;
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if searchController.active && searchController.searchBar.text != "" {
			return searchChats!.count;
		}
		
		return chats != nil ? chats!.count + 1 : 0 + 1;
	}
	
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if searchController.active && searchController.searchBar.text != "" {
			return openChat(searchChats![indexPath.row])
		} else if indexPath.row == 0 {
			performSegueWithIdentifier("newGroupSegue2", sender: self);
			return;
		}
		return openChat(chats![indexPath.row - 1]);
	}
	
	
	func openChat(chat: LocalChats?) {
		if let vc = JLBundleController.instantiateJLChatVC() as? InChatViewController {
			vc.view.frame = self.view.frame
			vc.thisChat = chat;
			vc.participants =  (chat?.favoritesInChat)!
			let chatSegue = UIStoryboardSegue(identifier: "newsEGUE", source: self, destination: vc, performHandler: { () -> Void in
				self.navigationController?.pushViewController(vc, animated: true)
			})
			
			self.prepareForSegue(chatSegue, sender: nil)
			chatSegue.perform()
		}
	}
	
	
	
	func testChats() {
		/*
		let protoChat2 = Chat();
		protoChat2.friendName = "Angela Florentini"
		protoChat2.lastMessage = ">> Cuando vuelves a venir a Panama?"
		protoChat2.languages = "🇪🇸/🇮🇹";
		protoChat2.lastMessageSent = "4:04pm"
		protoChat2.picPreview = "stock-pic3"
		chats.append(protoChat2);
		
		let protoChat = Chat();
		protoChat.friendName = "John Doe"
		protoChat.lastMessage = "I was thinking if I should travel to South America next month, what do you think?"
		protoChat.languages = "🇺🇸/🇪🇸";
		protoChat.lastMessageSent = "Saturday"
		protoChat.picPreview = "stock-pic1"
		chats.append(protoChat);
		
		
		let protoChat1 = Chat();
		protoChat1.friendName = "Michael Rogers"
		protoChat1.lastMessage = "Ti chiamo domani mattina."
		protoChat1.languages = "🇮🇹/🇺🇸";
		protoChat1.lastMessageSent = "5/23/16"
		protoChat1.picPreview = "stock-pic2"
		chats.append(protoChat1);
		*/
	}
	
	
}
