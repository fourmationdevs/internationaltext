//
//  SettingsTableViewController.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/23/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import UIKit
class SettingsTableViewController: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let imagePicker = UIImagePickerController();
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameField: UITextField!
	@IBOutlet weak var editButton: UIButton!
	
    @IBOutlet weak var statusCell: UITableViewCell!
    @IBOutlet weak var phoneNumberCell: UITableViewCell!
	@IBOutlet weak var DeleteCell: UITableViewCell!
	@IBOutlet weak var themeColorCell: UITableViewCell!

	@IBOutlet weak var languageButton: UITableViewCell!
	
	@IBOutlet weak var statusField: UITextField!
	
    var selectedLanguage = "";
    var languageCode = "";
    var hasLanguageBeenSet = false;

	
    @IBAction func unwindWithSelectedLanguage(segue:UIStoryboardSegue) {
        if let languagePickerViewController = segue.sourceViewController as? LanguageTableViewController {
            self.selectedLanguage = languagePickerViewController.languagueSelected;
            self.languageCode = languagePickerViewController.languageCode;
			languageButton.textLabel?.text = selectedLanguage;
            //languageButton.setNeedsDisplay();
            hasLanguageBeenSet = true;
            //errorLabel.hidden = true;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Settings";
        
        imagePicker.delegate = self;
        profileNameField.delegate = self;
        profileNameField.text = LocalUser.sharedInstance.userData.name;
        
        profileImageView.convertToCirclePicture(UIColor.blackColor().CGColor, width: 1);
        profileImageView.image = UIImage(data: LocalUser.sharedInstance.userData.profilePic!);
		
		editButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTap)))
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTap)));
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .Plain, target: self, action: #selector(saveButton));
        
        statusField?.text = LocalUser.sharedInstance.userData.statusMessage;
        phoneNumberCell.textLabel?.text = LocalUser.sharedInstance.userData.phoneNumber;
		
		languageButton.textLabel?.textColor = UIColor.blueColor();
        languageButton.textLabel!.text = "Language: " + LocalUser.sharedInstance.userData.languageCode!;
		
		themeColorCell.textLabel?.text = "Change Theme Color";
		themeColorCell.textLabel?.textColor = ColorManager.sharedInstance.currentThemeColor;
		
		DeleteCell.textLabel?.text = "DELETE"
		DeleteCell.textLabel?.textColor = UIColor.redColor();
		DeleteCell.userInteractionEnabled = true;
		DeleteCell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapDeleteCell)));
		
		profileNameField.tag = 402;
		statusField.tag = 401;
		statusField.delegate = self;
		
		
    }
	
	func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		
		if textField.tag == 402 {
			let limit = 32;
			
			guard let text = textField.text else { return true }
			let newLength = text.utf16.count + string.utf16.count - range.length
			return newLength <= limit // Bool
			
			
		} else if textField.tag == 401 {
			let limit = 80;
			guard let text = textField.text else { return true }
			let newLength = text.utf16.count + string.utf16.count - range.length
			return newLength <= limit // Bool
		}
		
		return true;
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		
		textField.resignFirstResponder();
		
		return true;
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		updateUIColor();
	}
	
	override func viewDidAppear(animated: Bool) {
	}
	
	func updateUIColor() {
	
		themeColorCell.textLabel?.textColor = ColorManager.sharedInstance.currentThemeColor;
		profileImageView.layer.borderColor = ColorManager.sharedInstance.currentThemeColor.CGColor;
	
	
	}
	
	func tapDeleteCell() {
		
		let message = "Do you wish to delete all your chat messages and conversations?";
		let alert = UIAlertController(title: "Confirmation", message: message, preferredStyle: .ActionSheet);
		
		//iPad stuff
		if let popoverPresentationController = alert.popoverPresentationController {
			popoverPresentationController.sourceView = DeleteCell.textLabel;
			popoverPresentationController.sourceRect = DeleteCell.textLabel!.frame;
		}
		
		let yesAction = UIAlertAction(title: "Yes", style: .Destructive) { (action) in
			
			LocalUser.sharedInstance.userData.chats?.removeAll();
			LocalUser.sharedInstance.userData.chats = [];
			LocalUser.sharedInstance.saveUser();
			DatabaseManager.instance.deleteAllMessages();
			print(LocalUser.sharedInstance.userData.chats?.count)
		}
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
			
		}
		
		alert.addAction(yesAction);
		alert.addAction(cancelAction);
		
		self.presentViewController(alert, animated: true, completion: nil);
	
	
	}

    func saveButton() {
		
		profileNameField.resignFirstResponder();
		statusField.resignFirstResponder();
		
        let name = profileNameField.text;
        let status = statusField.text;
		
        if name?.isEmpty == false {
			
			LocalUser.sharedInstance.userData.name = name!;
			if status == "" {
				LocalUser.sharedInstance.userData.statusMessage = nil;
			} else {
				LocalUser.sharedInstance.userData.statusMessage = status;
			}
			
			

			
            if hasLanguageBeenSet {
                //errorLabel.hidden = true;
                LocalUser.sharedInstance.userData.languageCode = languageCode;
			}
			
			LocalUser.sharedInstance.saveUser();
			
			RemoteData.sendUserData({ (data) in
				
				if self.profileImageView.image != nil {
					LocalUser.sharedInstance.userData.profilePic = UIImageJPEGRepresentation(self.profileImageView.image!, 1.0);
					RemoteData.uploadPicture();
				}
				self.displayerToast("Saved");
				LocalUser.sharedInstance.saveUser();
			})
		} else {
			displayerToast("Username is empty");
		}
    }
	
	func displayerToast(message: String) {
		print("Displaying toast with message: \(message)")
		dispatch_async(dispatch_get_main_queue(), {
			let alert = UIAlertController(title: nil, message: message, preferredStyle: .Alert);
			self.presentViewController(alert, animated: true, completion: nil);
			let duration : Int64 = 1; // duration in seconds
			dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (duration * 1000000000)), dispatch_get_main_queue(), {
				alert.dismissViewControllerAnimated(true, completion: nil);
			})
		})
	}
	
    func imageTap() {
        createActionSheet();
    }
    
    
    func createActionSheet() {
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .ActionSheet)
		
		if let popoverPresentationController = optionMenu.popoverPresentationController {
			popoverPresentationController.sourceView = profileImageView;
			popoverPresentationController.sourceRect = profileImageView.frame;
		}
        
        // 2
        let pickImageAction = UIAlertAction(title: "Choose from Photo Library", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Photo Library")
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .PhotoLibrary;
            //self.imagePicker.mediaTypes = [UTType ];
            
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
            
        })
        let takePictureAction = UIAlertAction(title: "Take Photo", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Take Photo")
			self.imagePicker.allowsEditing = true;
			self.imagePicker.sourceType = .Camera;
			self.imagePicker.cameraDevice = .Front;

			
			self.presentViewController(self.imagePicker, animated: true, completion: nil);
            //self.dismissViewControllerAnimated(false, completion: nil);
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
            //self.dismissViewControllerAnimated(false, completion: nil);
        })
        
		
        // 4
        optionMenu.addAction(pickImageAction)
        optionMenu.addAction(takePictureAction)
        optionMenu.addAction(cancelAction)

        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
	
	func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
		
		let image = info[UIImagePickerControllerEditedImage] as? UIImage;
		profileImageView.image = image;
		picker.dismissViewControllerAnimated(true, completion: nil);
		
	}
	
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        print("Did finish picking image");
        
        profileImageView.image = image;
        picker.dismissViewControllerAnimated(true, completion: nil);
        
    }
    
}
