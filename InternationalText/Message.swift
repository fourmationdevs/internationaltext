//
//  Message.swift
//  InternationalText
//
//  Created by Angelo - Mobile Developer on 6/8/16.
//  Copyright © 2016 Fourmation Inc. All rights reserved.
//

import Foundation
import JLChatViewController

class Message : JLMessage {
    
    
    var transTag = "";
    var ogTag = "";
    var ogText = "";
    var senderNumber = "";
    
    override init(text: String, senderID: String, messageDate: NSDate, senderImage: UIImage?) {
        super.init(text: text, senderID: senderID, messageDate: messageDate, senderImage: senderImage)
        
        transTag = "";
        ogTag = "";
        ogText = "";
        self.messageKind = MessageKind.Custom;
        senderNumber = ""
        
    }
    
    
    
}